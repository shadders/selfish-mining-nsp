#!/usr/bin/env bash

#set global env variables
. /root/pool/node-stratum-pool/conf/setenv.sh
#load rpc auth
. /root/set_rpc_auth.sh

#reset chain on the master node

. /root/pool/node-stratum-pool/reset-chain.sh

#reset bitcoin1 and bitcoin2
#cd /root/pool/node-stratum-pool/conf/templates/sm-coinds
#make stop
#make clean
#make start-isolated

#start bitcoind
/root/coinds/bitcoin-sm/src/bitcoind -daemon

#wait for it to start
echo waiting for bitcoind to start
cd /root/pool/node-stratum-pool/conf/templates
#node waitForDaemon.js $RPC_USER $RPC_PASSWORD
#should be quick first time - we can't use waitForDaemon until it thinks it's connected
#or it will just get stuck with "ERROR: Bitcoin is downloading blocks..."
sleep 5s

#generate the first block so other nodes see this one as valid
/root/coinds/bitcoin-sm/src/bitcoin-cli generate 1

echo "master-chain-generate-block1" > /root/state/mine_mode.txt
echo "`date -u` master-chain-generate-block1" >> /root/state/mine_mode_log.txt

. /root/pool/node-stratum-pool/stop-daemon.sh 3s

echo "Reset complete"

echo "master-chain-reset-complete" > /root/state/mine_mode.txt
echo "`date -u` master-chain-reset-complete" >> /root/state/mine_mode_log.txt




