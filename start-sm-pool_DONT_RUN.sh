#!/usr/bin/env bash

ROLE=`cat /root/state/role.txt`

if [ "$ROLE" = "pool" ]; then
    node init.js --sm  |& tee -a /root/state/pool.log
else
    echo "Server is not designated as selfish mining pool, aborting..."
fi