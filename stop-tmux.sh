#!/usr/bin/env bash

echo "stopping-tmux" > /root/state/mine_mode.txt
echo "`date -u` stopping-tmux" >> /root/state/mine_mode_log.txt


#ensure bitcoind isn't running
. /root/pool/node-stratum-pool/stop-daemon.sh 3s

#kill existing tmux session in case it exists
killall tmux

tmux start-server

echo "stopped" > /root/state/mine_mode.txt
echo "`date -u` stopped" >> /root/state/mine_mode_log.txt
