#!/usr/bin/env bash

echo "init-build-daemon" > /root/state/mine_mode.txt
echo "`date -u` init-build-daemon" >> /root/state/mine_mode_log.txt

. /root/pool/node-stratum-pool/conf/stop-tmux.sh

cd /root/coinds/bitcoin-sm
git reset --hard HEAD
git pull

echo "building-daemon" > /root/state/mine_mode.txt
echo "`date -u` building-daemon" >> /root/state/mine_mode_log.txt

make

echo "build-daemon-complete" > /root/state/mine_mode.txt
echo "`date -u` build-daemon-complete" >> /root/state/mine_mode_log.txt
