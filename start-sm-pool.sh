#!/bin/bash

NORESTART=$1

if [ $NORESTART = 'norestart' ]; then

    ./start-sm-pool_DONT_RUN.sh

else

    while true; do
        ./start-sm-pool_DONT_RUN.sh & q_pid="${!}"
        ( sleep 86400 ; kill "${q_pid}" ) & s_pid="${!}"
        wait "${q_pid}"
        kill "${s_pid}"
        wait "${s_pid}"
    done

fi