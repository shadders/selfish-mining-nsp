#!/usr/bin/env bash

echo "init-chain-reset" > /root/state/mine_mode.txt
echo "`date -u` init-chain-reset" >> /root/state/mine_mode_log.txt

#ensure bitoind isn't running
. /root/pool/node-stratum-pool/stop-daemon.sh 3s

#backup wallet
cp /root/.bitcoin/regtest/wallet.dat /root/.bitcoin/wallet.dat
rm -rf /root/.bitcoin/regtest
mkdir /root/.bitcoin/regtest
cp /root/.bitcoin/wallet.dat /root/.bitcoin/regtest/wallet.dat

if [ -e /root/state/blocks_found.json ]; then
    mkdir /root/state/blocks_found_backup
    $DATETIME=$(date '+%d/%m/%Y_%H:%M:%S')
    mv /root/state/blocks_found.json /root/state/blocks_found_backup/"$DATETIME".json
fi

echo "chain-reset-complete" > /root/state/mine_mode.txt
echo "`date -u` chain-reset-complete" >> /root/state/mine_mode_log.txt