/**
 * Created by Shadders on 15/06/2017.
 */

var args = process.argv.slice(2);

var fs = require('fs');
var Stratum = require('./lib/index.js');

process.sm_config_dir = './conf/';

//if (args.length > 0 && args[0] == '--dev')
process.is_dev = args.indexOf('--dev') > -1
if (process.is_dev)
    process.sm_config_dir = './conf/dev-sm/';

process.is_sm_pool = args.indexOf('--sm') > -1;

if (args.indexOf('--sm') > -1 && !process.is_dev)
    process.sm_config_dir = './conf/sm/';


var config_file = process.sm_config_dir + 'pool.json';


var poolConfig = JSON.parse(fs.readFileSync(config_file));
var coin = poolConfig.coin;

var authFn = function(ip, port , workerName, password, callback){ //stratum authorization function
    //console.log("Authorize " + workerName + ":" + password + "@" + ip);
    callback({
        error: null,
        authorized: true,
        disconnect: false
    });
};

var pool = Stratum.createPool(poolConfig, authFn);

/*
'data' object contains:
    job: 4, //stratum work job ID
        ip: '71.33.19.37', //ip address of client
    port: 3333, //port of the client
    worker: 'matt.worker1', //stratum worker name
    height: 443795, //block height
    blockReward: 5000000000, //the number of satoshis received as payment for solving this block
    difficulty: 64, //stratum worker difficulty
    shareDiff: 78, //actual difficulty of the share
    blockDiff: 3349, //block difficulty adjusted for share padding
    blockDiffActual: 3349 //actual difficulty for this block


//AKA the block solution - set if block was found
blockHash: '110c0447171ad819dd181216d5d80f41e9218e25d833a2789cb8ba289a52eee4',

    //Exists if "emitInvalidBlockHashes" is set to true
    blockHashInvalid: '110c0447171ad819dd181216d5d80f41e9218e25d833a2789cb8ba289a52eee4'

//txHash is the coinbase transaction hash from the block
txHash: '41bb22d6cc409f9c0bae2c39cecd2b3e3e1be213754f23d12c5d6d2003d59b1d,

error: 'low share difficulty' //set if share is rejected for some reason
*/
pool.on('share', function(isValidShare, isValidBlock, data){

    if (isValidBlock) {
        console.log('Block found');
        logBlock(data);
    } else if (isValidShare) {
        //console.log('Valid share submitted');
    } else if (data.blockHash)
        console.log('We thought a block was found but it was rejected by the daemon');
    else
        console.log('Invalid share submitted')

    //console.log('share data: ' + JSON.stringify(data));
});



/*
 'severity': can be 'debug', 'warning', 'error'
 'logKey':   can be 'system' or 'client' indicating if the error
 was caused by our system or a stratum client
 */
pool.on('log', function(severity, logText){
    console.log('[' + severity + ']: ' + logText);
});

var blocksFoundFile = '/root/state/blocks_found.json';

if (!process.is_dev) {
    //ensure the blocks_found file exists
    if (!fs.existsSync(blocksFoundFile)) {
        fs.appendFileSync(blocksFoundFile, '');
    }
}

pool.start();

function logBlock(blockData) {
    var outfile = process.is_dev ? '../blocks_found.json' : blocksFoundFile;
    var date = new Date();
    blockData.timeStamp = date.getTime();
    blockData.utcTime = date.toUTCString();
    var json = JSON.stringify(blockData) + "\n";
    fs.appendFileSync(outfile, json)
}