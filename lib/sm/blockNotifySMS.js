/**
 * Created by shadders on 9/04/18.
 */
const fs = require('fs');

// Twilio Credentials
const accountSid = 'ACf001a5c60e6cd5d985bc0990601630f4';
const authToken = '1f5aa226ecb63b81b64adc799b368a7b';

// require the Twilio module and create a REST client
const client = require('twilio')(accountSid, authToken);

var smsNotify = module.exports = function() {

    let _this = this;
    this.BLOCK_INTERVAL = 500;


    this.notifyBlockHeight = function (height) {
        height--; // for some reason height is 1 greater than reported by getinfo
        blocksLeft = this.BLOCK_INTERVAL - height % _this.BLOCK_INTERVAL;
        if (blocksLeft <= 50) {
            if (blocksLeft <= 5 || blocksLeft % 10 == 0) {
                doNotify(height, blocksLeft);
            }
        } else if (blocksLeft % 100 == 0) {
            doNotify(height, blocksLeft);
        }
    }

    function doNotify(height, blocksLeft) {

        var msg = "Found block " + height + " we are " + blocksLeft + " blocks from difficulty adjustment";

        client.messages.create(
            {
                to: '+61412263822',
                from: '+61448072183',
                body: msg,
            },
            (err, message) => {
                console.log(message);
                console.log(JSON.stringify(err));
            }
        );
    }

}