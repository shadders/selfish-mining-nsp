/**
 * Created by shadders on 30/07/17.
 */

var p2p = require('bitcore-p2p')
var bitcore = require('bitcore-lib');
var BufferWriter = bitcore.encoding.BufferWriter;
var Buffers = bitcore.encoding.Buffers;
var Networks = bitcore.Networks;
Networks.enableRegtest();
Networks.defaultNetwork = Networks.testnet;
var Peer = p2p.Peer;
var Messages = p2p.Messages;
var messages = new Messages();
var BufferUtil = bitcore.util.buffer;

module.exports = function(pool, _logger) {

    var _this = this;
    var logger = _logger;

    var seenHashes = {};
    var seenHeights = {};
    var missingHeights = {};
    var headerMessagesByHash = {};

    /**
     * initialize the cache cleaner
     */
    setInterval(function () {

        var time = (new Date()).getTime();
        var timeout = 86400 * 1000 * 2; // 48 hours

        var newSeenHashes = {};
        for (var hash in seenHashes) {
            var obj = seenHashes[hash];
            //24 hour timeout
            if (time < obj.lastSeen + timeout) {
                newSeenHashes[hash] = obj;
            }
        }
        seenHashes = newSeenHashes;

        var newSeenHeights = {};
        for (var height in seenHeights) {
            var heights = seenHeights[height];
            var newHeights = {};
            for (var hash in heights) {
                var obj = heights[hash];
                //24 hour timeout
                if (time < obj.lastSeen + timeout) {
                    newHeights[hash] = obj;
                }
            }
            if (Object.keys(newHeights).length > 0) {
                newSeenHeights[height] = newHeights;
            }
        }
        seenHeights = newSeenHeights;


        var newHeaderMessagesByHash = {};
        for (var hash in headerMessagesByHash) {
            var obj = headerMessagesByHash[hash];
            if (time < obj.lastSeen + timeout) {
                newHeaderMessagesByHash[hash] = obj;
            }

        }
        headerMessagesByHash = newHeaderMessagesByHash;

    }, 2000);

        this.reverseBufferToString = function reverseBufferToString(buf) {
        if (typeof buf == 'string')
            return buf;
        buf = BufferUtil.reverse(buf)
        return buf.toString('hex');

        this.reverseStringToBuffer = function (str) {
                buf = new Buffer(str, 'hex')
            buf = BufferUtil.reverse(buf)
            return buf;

        }
    }

    /**
     * Runs rpc commands on both daemons and executes callback once both are complete
     *
     * @param privateBatchCommands
     * @param publicBatchCommands
     * @param callback a callback that should take params: errorPrivate, resultPrivate, errorPublic, resultPublic
     */
    this.doubleRPCCall = function (privateBatchCommands, publicBatchCommands, callback) {

        var sidesComplete = 0;

        var errorPrivate, errorPublic, resultsPrivate, resultsPublic;

        function _onPrivateComplete(error, results) {
            errorPrivate = error;
            resultsPrivate = results;
            _handleResult('private', error, results);
        }

        function _onPublicComplete(error, results) {
            errorPublic = error;
            resultsPublic = results;
            _handleResult('public', error, results);
        }

        function _handleResult(side, error, result) {
            sidesComplete++;
            if (sidesComplete == 2) {
                callback(errorPrivate, resultsPrivate, errorPublic, resultsPublic);
            }
        }

        pool.private_daemon.batchCmd(privateBatchCommands, _onPrivateComplete);
        pool.public_daemon.batchCmd(publicBatchCommands, _onPublicComplete);
    }

    /**
     * Add block hash to getinfo
     * @param privateInfo
     * @param publicInfo
     * @param callback
     */
    this.rpcGetBlockHashBothSides = function(privateInfo, publicInfo, callback) {

        var getBlockHashCommandsPrivate = [["getblockhash", [privateInfo.blocks]]];
        var getBlockHashCommandsPublic = [["getblockhash", [publicInfo.blocks]]];

        function _onBlockHashesReturned(errorPrivate, resultsPrivate, errorPublic, resultsPublic) {

            if (errorPrivate) {
                privateInfo = null;
            } else {
                privateInfo.hash = resultsPrivate[0].result;
            }

            if (errorPublic) {
                publicInfo = null;
            } else {
                publicInfo.hash = resultsPublic[0].result;
            }

            callback(privateInfo, publicInfo);
        }

        _this.doubleRPCCall(getBlockHashCommandsPrivate, getBlockHashCommandsPublic, _onBlockHashesReturned);
    }

    /**
     * getinfo to both chains
     * @param callback should take params: privateInfo, publicInfo
     */
    this.rpcGetinfoBothSides = function (callback) {

        var getinfoCommands = [["getinfo", []]];

        function _onHeightsReturned(errorPrivate, resultsPrivate, errorPublic, resultsPublic) {

            var privateInfo = null;
            var publicInfo = null;

            if (resultsPrivate) {
                privateInfo = resultsPrivate[0].result;
            } else {
                logger.log('RPC Error on getinfo: ' + errorPrivate.message);
            }
            if (resultsPublic) {
                publicInfo = resultsPublic[0].result;
            } else {
                logger.log('RPC Error on getinfo: ' + errorPublic.message);
            }

            if (privateInfo && publicInfo) {
                _this.rpcGetBlockHashBothSides(privateInfo, publicInfo, callback);
            } else {
                callback(privateInfo, publicInfo);
            }
        }

        _this.doubleRPCCall(getinfoCommands, getinfoCommands, _onHeightsReturned);
    }

    /**
     * @param message
     * @return {*} number of hashes in the message
     */
    this.getHashCountForMessage = function (message) {
        if (message.command == 'getdata') {
            return message.inventory.length;
        } else if (message.command == 'headers') {
            return message.headers.length;
        } else if (message.command == 'inv') {
            return message.inventory && message.inventory.length ? message.inventory.length : 0;
        } else if (message.command == 'getheaders') {
            return message.starts.length;
        } else if (message.command == 'block') {
            return 1;
        }
        return 0;
    }

    this.getPrevHashForMessage = function (message, index = 0) {

        if (message.prevHash)
            return message.prevHash;

        var len = _this.getHashCountForMessage(message);
        if (index + 1 > len)
            return null;

        var bufHash = null;
        if (message.command == 'headers') {
            bufHash = message.headers[index].prevHash;
        } else if (message.command == 'block') {
            bufHash = message.block.header.prevHash;
        }
        if (!bufHash)
            return null;
        var hash = _this.reverseBufferToString(bufHash);
        message.prevHash = hash;
        return hash;
    }

    /**
     * Determines if a message is a type that references one of more block hashes
     * @param message
     * @return {boolean}
     */
    this.isHashedMessage = function (message) {
        return _this.getHashCountForMessage(message) > 0;
    }

    this.getHashForMessage = function (message, index = 0) {

        if (message.hash && index == 0)
            return message.hash;

        var len = _this.getHashCountForMessage(message);
        if (index + 1 > len)
            return null;

        var bufHash = null;
        if (message.command == 'getdata') {
            bufHash = message.inventory[index].hash;
        } else if (message.command == 'headers') {
            bufHash = message.headers[index]._getHash();
        } else if (message.command == 'inv') {
            bufHash = message.inventory[index].hash;
        } else if (message.command == 'getheaders') {
            bufHash = message.starts[index];
        } else if (message.command == 'block') {
            bufHash = message.block.header._id;
        }
        if (!bufHash)
            return null;
        var hash = _this.reverseBufferToString(bufHash);
        if (index == 0)
            message.hash = hash;
        return hash;
    }

    /**
     * Attempts to determine the block height of a message.  If the message contains multiple hashes it will determine the first.
     *
     * It is safer in this case to split the message into multiple single hash messages.
     *
     * @param message
     * @return {*}
     */
    this.maybeGetBlockHeightForMessage = function(message) {
        return _maybeGetBlockHeightForMessage(message);
    }


    function _maybeGetBlockHeightForMessage(message) {
        if (message.height > 0)
            return message.height;
        var hash = _this.getHashForMessage(message)
        var prevHash = _this.getPrevHashForMessage(message);
        //var len = getHashCountForMessage(message);
        return _maybeGetBlockHeight(hash, prevHash);
    }

    /**
     * Attempt to determine the block height of a given hash either from a previously cached version of the block hash
     * or from the previous hash.  Aside from early in the life of the pool when RPC calls are used to determine block
     * heights, they are usually determined by the previous hash already having a block height so it's best to always
     * pass the prevHash parameter if you have it available.
     *
     * For this reason inv messages will rarely be able to have height determined.  Once a headers message has been seen
     * it's fairly safe to assume block height will be able to be determined except in rare instances where they may arrive
     * out of order.
     *
     * @param hash
     * @param prevHash if known
     * @return {*}
     */
    this.maybeGetBlockHeight = function(hash, prevHash = null) {
        return _maybeGetBlockHeight(hash, prevHash);
    }

    /**
     * Checks any
     */
    this.checkMissingHeights = function() {
        //check if we can update any that are missing heights
        if (checkMissingHeights) {
            for (var missingHash in missingHeights) {
                var obj = missingHeights[missingHash];
                if (obj.height) {
                    delete missingHeights[missingHash];
                } else { //avoid recursive loop
                    var missingHeight = _maybeGetBlockHeight(obj.hash, obj.prevHash);
                    if (missingHeight) {
                        obj.height = missingHeight;
                        _addSeenHeight(obj.height, obj);
                        delete missingHeights[missingHash];
                    }
                }
            }
        }
    }

    /**
     * double check that our blockheight is correct
     * @param hash
     * @param height
     */
    function validateBlockHeight(hash, prevHash, height) {

        //var stack = new Error().stack;

        var getblockCommand = [['getblock', [hash]]];

        var validateHeightFunction = function(errorPrivate, resultPrivate, errorPublic, resultPublic) {
            var result = false;
            var rpcErrors = 0;
            var ph = prevHash;//so I can see it in debug easily
            var privateHeight = null;
            var publicHeight = null;
            var realHeight
            if (resultPrivate && resultPrivate[0].result) {
                privateHeight = resultPrivate[0].result.height;
                if (privateHeight == height) {
                    result = true;
                }
                realHeight = privateHeight;
            } else {
                rpcErrors++;
            }
            if (!result && resultPublic && resultPublic[0].result) {
                publicHeight = resultPublic[0].result.height;
                if (publicHeight == height) {
                    result = true;
                }
                realHeight = publicHeight;
            } else {
                rpcErrors++;
            }
            if (!result && rpcErrors < 2) {
                //var stackTrack = stack
                throw new Error('failed height validation for hash: ' + hash + ' our height: ' + height + ' real height: ' + realHeight);
            }
        }
        _this.doubleRPCCall(getblockCommand, getblockCommand, validateHeightFunction);
    }

    function _maybeGetBlockHeight(hash, prevHash = null) {

        var obj = _getSeenHash(hash);
        if (obj && obj.height && !isNaN(obj.height)) {
            return obj.height;
        }

        if (prevHash != null) {
            var height = _maybeGetBlockHeight(prevHash, null);
            if (!isNaN(height) && height !== null && height > 0) {
                height = height + 1;
                _addSeenHash(hash, height, null);
                validateBlockHeight(hash, prevHash, height);
                return height;
            }
        }

        return null;
    }

    this.getSeenHash = function(hash) {
        return _getSeenHash(hash);
    }

    function _getSeenHash(hash) {
        if (hash in seenHashes) {
            return seenHashes[hash];
        }
        return null;
    }

    function _addSeenHeight(height, obj) {
        var heights;
        if (height in seenHeights) {
            heights = seenHeights[height];
        } else {
            heights = {};
            seenHeights[height] = heights;
        }
        heights[obj.hash] = obj;
    }

    this.getSeenHeight = function(height) {
        if (height in seenHeights) {
            return seenHeights[height];
        }
        return null;
    }

    /**
     * adds a hash to an internal map used for determining block height
     * @param hash
     * @param height
     * @param prevHash if known, otherwise pass null.
     * @param message
     * @param peer
     */
    this.addSeenHash = function(hash, height, prevHash = null, message = null, peer = null, chain = null) {
        return _addSeenHash(hash, height, prevHash, message, peer, chain);
    }

    function _addSeenHash(hash, height, prevHash = null, message = null, peer = null, chain = null) {
        if (!height)
            height = _maybeGetBlockHeight(hash, prevHash);
        if (!height && message)
            height = _maybeGetBlockHeightForMessage(message);

        if (!chain && peer)
            chain = peer.chain;

        var obj;
        if (hash in seenHashes) {
            //add height and message if it's missing
            obj = seenHashes[hash];
            if (!obj.message && message && message.command == 'headers') {
                message = _splitHeadersMessage(message, peer)[0];
                obj.message = message;
            }
            if (height && !isNaN(height)) {
                obj.height = height;
            }
            if (prevHash) {
                if (obj.prevHash && prevHash != obj.prevHash)
                    throw new Error('prevHash mismatch');
                obj.prevHash = prevHash;
            }
        } else {
            obj = {
                'hash': hash,
                'height': height,
                'prevHash': prevHash,
                'message': message,
                'seenPublic': false,
                'seenPrivate' : false,
                'firstSeenBy' : chain
            }
        }
        if (chain == 'private')
            obj.seenPrivate = true;
        else if (chain == 'public')
            obj.seenPublic = true;

        obj.lastSeen = (new Date()).getTime();
        seenHashes[hash] = obj;
        if (obj.height > 0)
            _addSeenHeight(obj.height, obj);

        if (!obj.height) {
            missingHeights[hash] = obj;
        }

    }

    /**
     * Splits headers message into multipl messages all containing a single header entry
     * @param message
     */
    this.splitHeadersMessage = function(message, peer) {
        return _splitHeadersMessage(message, peer);
    }

    function _splitHeadersMessage(message, peer) {
        let messageList = [];
        for (var header of message.headers) {
            var newMessage = messages.Headers();
            newMessage.headers = [header];
            newMessage.hash = _this.getHashForMessage(newMessage);
            newMessage.prevHash = _this.getPrevHashForMessage(newMessage);
            newMessage.height = _maybeGetBlockHeight(newMessage.hash, newMessage.prevHash);
            newMessage.peer = peer;
            newMessage.receivedBy = peer.chain;
            messageList.push(newMessage);
        }
        //we want to process lowest block heights first
        messageList.sort(function (a, b) {
            if (a.height === null)
                a.height = _maybeGetBlockHeight(a.hash, a.prevHash);
            if (b.height === null)
                b.height = _maybeGetBlockHeight(b.hash, b.prevHash);
            return a - b;
        });
        return messageList;
    }

    this.getMessageOutput = function (peer, message, targetPeer = null) {

        var height = _this.maybeGetBlockHeightForMessage(message);
        var hash = _this.getHashForMessage(message);
        var len = _this.getHashCountForMessage(message);

        if (targetPeer == null)
            targetPeer = peer.pairedPeer;

        var txt = peer.name + " --> " + targetPeer.name + ": " + message.command + '[' + len + ']';

        if (len <= 1) {
            height = height ? height : '???';
            hash = hash ? hash : 'no hash';
            txt += ' [' + height + ']: ' + hash;
        } else {

            var heightsTxt = ' [';
            var startLen = heightsTxt.length;
            var firstHash = null;
            var lastHash = null;

            for (var i = 0; i < len; i++) {

                if (heightsTxt.length > startLen)
                    heightsTxt += ', ';

                var iHash = _this.getHashForMessage(message, i);
                var iPrevHash = _this.getPrevHashForMessage(message, i);
                var iHeight = _this.maybeGetBlockHeight(iHash, iPrevHash);
                iHeight = iHeight ? iHeight : '???';
                heightsTxt += iHeight;

                if (i == 0)
                    firstHash = iHash;
                else if (i == len - 1)
                    lastHash = iHash;
            }
            heightsTxt += '] ';
            txt += heightsTxt + firstHash + ' ... ' + lastHash;
        }
        return txt;
    }

    this.getSuppressedMessageOutput = function (peer, message) {
        var txt = getMessageOutput(peer, message);
        txt += " - suppressed";
        return txt;
    }

    this.cacheHeadersMessage = function(hash, message) {
        var obj = {
            'lastSeen': (new Date()).getTime(),
            'message': message
        }
        headerMessagesByHash[hash] = obj;
    }

    this.getCachedHeadersMessage = function(hash) {
        if (hash in headerMessagesByHash) {
            return headerMessagesByHash[hash].message;
        }
        return null;
    }


}



