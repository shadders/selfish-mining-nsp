/**
 * Created by shadders on 19/07/17.
 */

/**
 * Created by shadders on 13/06/17.
 */
var fs = require('fs');

var p2p = require('bitcore-p2p');
var bitcore = require('bitcore-lib');

var Peer = p2p.Peer;
var Messages = p2p.Messages;
var messages = new Messages();
var BufferUtil = bitcore.util.buffer;

var selfishMiningPeers = require('./SmPeers.js');
var smFunctions = require('./smFunctions.js');
var smfunc = null;

var hasBeenMarkedSafe = false;
var numRPCBatchesComplete = 0;
var _smPeers;


/**
 *
 * @param source descriptive string of where the block info was sourced, either method name, rpc, message etc
 * @param chain 'public' or 'private'
 * @param hash block hash
 * @param height height of blok
 * @param message the message associated, this should always be a headers message.  Once the chain is initialized this is
 * mandatory.
 * @param validated whether we are certain this block is the one the peer has accepted.
 There's a short window between when we see the 'headers' message (which is when create this object)
 and when the peer gets to see the block itself.  During that time it's likely mining on top the previous
 block so we need to not prune the start of the chains past the first validated block until this one
 is validated.
 * @constructor
 */
var BlockMeta = function (source, chain, hash, height, message, validated) {

    if (!height) {
        height = smfunc.maybeGetBlockHeight(hash);
    }
    if (!height && message) {
        height = smfunc.maybeGetBlockHeightForMessage(message);
    }

    var error = null;
    if (!(typeof hash == 'string'))
        error = 'hash is not a string: ' + hash;
    if (hash == null)
        error = 'hash is null';
    if (hash && hash.length != 64)
        error = 'hash is not 64 chars: ' + hash;
    if (isNaN(height) || height === null)
        error = 'height is not a number: ' + height;
    if (message && message.command != 'headers')
        error = 'message is not type headers';
    if (error)
        throw new Error(error);
    this.hash = hash;
    this.height = parseInt(height);
    this.source = source;
    this.chain = chain;

    //whether we are certain this block is the one the peer has accepted.
    //There's a short window between when we see the 'headers' message (which is when create this object)
    //and when the peer gets to see the block itself.  During that time it's likely mining on top the previous
    //block so we need to not prune the start of the chains past the first validated block until this one
    //is validated.
    this.validated = validated;

    //whether this block matches the same height in the other chain.
    // == : yes
    // != : no
    // -- : haven't seen this block height in the other chain
    // ?? : unknown
    this.matchStatus = '??';

    if (message) {
        this.message = message;

        // if (message.receivedBy != chain)
        //     throw new Error('chain mismatched with receivedBy');

        this.prevHash = _smPeers.getPrevHashForMessagePublic(message);


    } else if (hasBeenMarkedSafe && numRPCBatchesComplete >= 2) {
        //var state = SelfMManager.getValidState(true);
        throw new Error('creating blockmeta without message after safeToMine');
    }

    var thisM = this;
    this.clone = function() {
        return new BlockMeta(thisM.source, thisM.chain, thisM.hash, thisM.height, thisM.message, thisM.validated);
    }

}

var TimedLogger = function () {

    var lastLogTime = (new Date()).getTime();

    this.log = function (message) {
        var currentTime = (new Date()).getTime();
        var diff = currentTime - lastLogTime;
        var time = diff + 'ms] ';
        var out = '[';
        while (out.length < 8 - time.length)
            out += ' ';

        var dateString = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

        out += time + message;
        console.log("[" + dateString + "] " + out);
        lastLogTime = currentTime;
    }

}

var SelfMManager = module.exports = function SelfManager(_pool, privateStartHeight) {

    var _this = this;
    var pool = _pool;
    var logger = new TimedLogger();
    this.getLogger = function () {
        return logger
    };
    smfunc = new smFunctions(pool, logger);

    var publicByHeight = {};
    var publicByHash = {};
    var publicUnconnected = {};

    var privateByHeight = {};
    var privateByHash = {};
    var privateUnconnected = {};

    var unvalidatedLength = 0;

    var allHeights = {};


    var equalised = false;
    var trimmedToCommonBase = false;

    this.newBlockMeta = function (source, chain, hash, height, message, validated) {
        return new BlockMeta(source, chain, hash, height, message, validated);
    }

    var smPeers = new selfishMiningPeers(pool, this, logger, privateStartHeight, smfunc);
    _smPeers = smPeers;

    smPeers.startupChainSync(function () {
        //on complete
        logger.log("Chains initialized: " + asShortString());
    });

    this.getPeers = function () {
        return smPeers;
    }

    this.getValidState = function (pauseOnMismatchedBase = true) {
        return validateState(pauseOnMismatchedBase);
    }

    /**
     * Validates the state of the chain... This is kind of a unit test which should catch any bugs in our
     * state storage implementation.  It will also tell us if the chain is in a safe state to use for making
     * SM decisions.
     *
     * - ensure both chains have common base
     * - ensure byHashes has same entries as byHeights
     * - ensure allHeights contains all heights and no extras
     * - check if any blocks are missing a block message
     */
    function validateState(pauseOnMismatchedBase = true) {


        var validateResult = {};
        //whether the first entry in private and public chain are the same block height with matching hashes
        validateResult.isCommonBase = false;
        //whether byHeight maps contain the same entries as byHash - if they don't we've got a bug somewhere
        validateResult.byHeightByHashMatch = true;
        //whether any blocks that aren't the same on both chain do not contain the full block message
        //those are blocks where we may need to transmit to the other side on a future state change
        validateResult.hasDiffBlockWithNoMessage = false;
        //inverted version of the above that we'll use for final output so a valid state is all 'true'
        validateResult.allBlocksHaveMessage = null;
        //whether allHeights contains the same set of entries and public and private combined
        validateResult.isAllHeightsMatched = true;
        // //whether each chain has matching prevHash with prior entry, this can't be true unless hasDiffBlockWithNoMessage is false
        // validateResult.isChainsConsistent = true;

        validateResult.isInternallyConsistent = null;
        validateResult.isSafeToMine = null;

        var maps = {
            'private': {
                'byHash': privateByHash,
                'byHeight': privateByHeight,
                'unconnected': privateUnconnected,
                'ends': getEnds(privateByHeight)
            },
            'public': {
                'byHash': publicByHash,
                'byHeight': publicByHeight,
                'unconnected': publicUnconnected,
                'ends': getEnds(publicByHeight)
            }
        };
        maps.private.other = maps.public;
        maps.public.other = maps.private;

        //ensure both chains have common base
        validateResult.isCommonBase = checkMatchingBase(pauseOnMismatchedBase);

        //clone allHeights so we can delete as we find them in byHeight and byHash
        var allBlocks = Object.assign({}, allHeights);
        var deleted = {};


        for (var chain in maps) {
            var map = maps[chain];
            var sizeByHash = 0;
            var sizeByHeight = 0;

            //confirm all blocks in byHeight are in byHash
            var lastHeight = null;
            for (var height in map.byHeight) {
                sizeByHeight++;

                //delete from allBlocks
                if (height in allBlocks) {
                    delete allBlocks[height];
                    deleted[height] = 0;
                } else {
                    if (!(height in deleted)) {
                        //allBlocks is missing this height so we're in and inconsistent that
                        validateResult.isAllHeightsMatched = false;
                    }
                }

                var byHeightBlock = map.byHeight[height];
                var byHashBlock = byHeightBlock.hash in map.byHash ? map.byHash[byHeightBlock.hash] : null;
                var otherChainBlock = height in map.other.byHeight ? map.other.byHeight[height] : null;

                var matchesOtherBlock = otherChainBlock && otherChainBlock.hash == byHeightBlock.hash;

                if (matchesOtherBlock) {
                    byHeightBlock['matchStatus'] = '==';
                } else if (!otherChainBlock) {
                    byHeightBlock['matchStatus'] = '--';
                } else {
                    //different blocks
                    byHeightBlock['matchStatus'] = '!=';
                }

                //check if it has a message.  It should have one if it's different from the other chain's
                if (otherChainBlock && !matchesOtherBlock) {
                    if (byHeightBlock.hasOwnProperty('message') && byHeightBlock.message) {
                        //NOOP
                    } else {
                        validateResult.hasDiffBlockWithNoMessage = true;
                        // validateResult.isChainsConsistent = false;
                    }
                } else {
                    if (!otherChainBlock) {
                        if (!byHeightBlock.hasOwnProperty('message') || !byHeightBlock.message) {
                            validateResult.hasDiffBlockWithNoMessage = true;
                            // validateResult.isChainsConsistent = false;
                        }
                    } else {
                        //same block so we can steal the other block's message if it has one
                        if (!byHeightBlock.hasOwnProperty('message') || !byHeightBlock.message) {
                            if (otherChainBlock && otherChainBlock.hasOwnProperty('message') && otherChainBlock.message) {
                                byHeightBlock.message = messages.Headers();
                                byHeightBlock.message.headers = [];
                                for (var header of otherChainBlock.message.headers) {
                                    var headerHash = BufferUtil.reverse(header._getHash()).toString('hex');
                                    if (headerHash == byHeightBlock.hash) {
                                        byHeightBlock.message.headers.push(header);
                                    }
                                }
                                if (byHeightBlock.message.headers.length == 0)
                                    throw new Error("didn't find matching header in otherChainBlock");

                            } else {
                                validateResult.hasDiffBlockWithNoMessage = true;
                                // validateResult.isChainsConsistent = false;
                            }
                        }
                    }
                }


                if (!byHashBlock) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }
                if (byHashBlock.height != byHeightBlock.height) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }
                if (byHashBlock.hash != byHeightBlock.hash) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }

                lastHeight = height;
            }

            //repeat for byHash - some of this is probably redundant but I'd rather waste a few cycles
            //to add certainty we've checked it properly.
            for (var hash in map.byHash) {
                sizeByHash++;

                var byHashBlock = map.byHash[hash];
                var height = byHashBlock.height;
                var otherChainBlock = hash in map.other.byHash ? map.other.byHash[hash] : null;

                //check if it has a message
                if (!byHashBlock.hasOwnProperty('message') || !byHashBlock.message) {
                    if (!otherChainBlock || !otherChainBlock.hasOwnProperty('message') || !otherChainBlock.message) {
                        validateResult.hasDiffBlockWithNoMessage = true;
                    }
                }

                //delete from allBlocks
                if (height in allBlocks) {
                    delete allBlocks[height];
                    deleted[height] = 0;
                } else {
                    if (!(height in deleted)) {
                        //allBlocks is missing this height so we're in and inconsistent that
                        validateResult.isAllHeightsMatched = false;
                    }
                }

                var byHeightBlock = byHashBlock.height in map.byHeight ? map.byHeight[byHashBlock.height] : null;
                if (!byHeightBlock) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }
                if (byHashBlock.height != byHeightBlock.height) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }
                if (byHashBlock.hash != byHeightBlock.hash) {
                    validateResult.byHeightByHashMatch = false;
                    break;
                }
            }

            if (sizeByHeight != sizeByHash) {
                validateResult.byHeightByHashMatch = false;
                //chains may not be consistent as we didn't check all entries in byHash
                validateResult.isChainsConsistent = false;
            }

        }

        if (Object.keys(allBlocks).length != 0) {
            //there's leftover blocks in allBlocks that aren't in byHeight or byHash
            validateResult.isAllHeightsMatched = false
        }

        validateResult.isInternallyConsistent =
            validateResult.byHeightByHashMatch
            && validateResult.isAllHeightsMatched
            && validateResult.isCommonBase;

        validateResult.isSafeToMine =
            validateResult.isInternallyConsistent
            && !validateResult.hasDiffBlockWithNoMessage;
        // && validateResult.isChainsConsistent;

        //invert this so a valid state is all 'true'
        validateResult.allBlocksHaveMessage = !validateResult.hasDiffBlockWithNoMessage;
        delete validateResult.hasDiffBlockWithNoMessage;

        if (validateResult.isSafeToMine)
            hasBeenMarkedSafe = true;

        return validateResult;
    }

    function checkMatchingBase(pause = true) {
        var privateBase = getBase(privateByHeight);
        var publicBase = getBase(publicByHeight);
        if (!privateBase || !publicBase) {
            //this is ok because the chains haven't synced yet.
            return true;
        }
        if (privateBase.height != publicBase.height) {
            //also ok
            return true;
        }
        if (privateBase.hash != publicBase.hash && pause) {
            //this is an invalid state, need to catch it.
            return false;
        }
        return true;
    }

    function checkEqualised() {
        if (equalised)
            return true;
        //var state = getState();

        if (Object.keys(privateByHeight).length == 0 || Object.keys(publicByHeight).length == 0)
            return false;

        if (!isContiguous(privateByHeight) || !isContiguous(publicByHeight))
            return false;

        //check they have a common base
        var common = false;
        for (var indexPrivate in privateByHeight) {
            if (indexPrivate in publicByHeight) {
                common = true;
                break;
                ;
            }
        }
        if (!common)
            return false;

        equalised = true;
        return true;
    }

    function trimToCommonBase() {

        var basePrivate = getBase(privateByHeight);
        var basePublic = getBase(publicByHeight);

        if (basePrivate.height == basePublic.height && basePrivate.hash == basePublic.hash) {
            trimmedToCommonBase = true;
            return;
        }
        trimmedToCommonBase = false;


        if (!checkEqualised())
            throw new Error('cannot trim start to common until equalised');
        var ordered = getSortedKeys(allHeights);


        for (var index of ordered) {
            if (index in privateByHeight && index in publicByHeight) {
                break;
            }

            var blockMeta = getByHeight(index, 'any');
            removeFromChain(blockMeta, 'public', false);
            removeFromChain(blockMeta, 'private', false);
        }
        trimmedToCommonBase = true;

    }

    function pruneChainBase() {

        var sorted = getSortedKeys(allHeights);

        var lastHeight = null;

        var prunedPrivate = [];
        var prunedPublic = [];

        var refreshables = [];

        var highestmatch = null;

        var foundConfirmed = false;
        var numUnConfirmed = 0;

        //first pass determine the highest matching block
        for (var height of sorted) {

            height = parseInt(height);
            var privateBlock = getByHeight(height, 'private');
            var publicBlock = getByHeight(height, 'public');

            //sanity check
            if (!privateBlock || !publicBlock) {
                var state = asShortString();
                if (lastHeight == null) {
                    var state = getState();
                    throw new Error('illegal state: allheights does not match chain maps');
                }
            }

            var matched = privateBlock && publicBlock && privateBlock.hash == publicBlock.hash;
            var confirmed = privateBlock && publicBlock && privateBlock.validated && publicBlock.validated;

            if (matched) {

                if (hasBeenMarkedSafe) {
                    if (confirmed) {
                        highestmatch = height;
                    }
                } else {
                    highestmatch = height;
                }

                //track the number of unconfirmed blocks at the start of the chain
                if (!foundConfirmed) {
                    if (confirmed)
                        foundConfirmed = true;
                    else
                        numUnConfirmed++;
                }

            } else {
                refreshables.push(height);
            }

            lastHeight = height;
        }

        unvalidatedLength = numUnConfirmed;

        //second pass delete all blocks before heighest matching
        if (highestmatch) {
            for (var height of sorted) {
                height = parseInt(height);

                if (height < highestmatch) {

                    var privateBlock = getByHeight(height, 'private');
                    var publicBlock = getByHeight(height, 'public');

                    removeFromChain(privateBlock, 'private', false);
                    removeFromChain(publicBlock, 'public', false);

                    //this is just for state tracking in debug
                    prunedPrivate.push(privateBlock);
                    prunedPublic.push(publicBlock);
                }

            }
        }

        if (!hasBeenMarkedSafe) {
            //refresh the hashes in case they changed due to reorg.
            getHeightsFromRPC(refreshables, 'public');
            getHeightsFromRPC(refreshables, 'private');
        } else {
            var state = getState();
            checkMatchingBase();
        }


    }

    function getByHeight(height, chain) {
        if (chain == 'public') {
            return publicByHeight[height];
        } else if (chain == 'private') {
            return privateByHeight[height];
        } else if (chain == 'any') {
            if (height in publicByHeight)
                return publicByHeight[height];
            if (height in privateByHeight)
                return privateByHeight[height];
            return null;
        }
        throw new Error('invalid chain specified: ' + chain);
    }

    function isInChain(height, chain) {
        if (chain == 'public') {
            return height in publicByHeight;
        } else if (chain == 'private') {
            return height in privateByHeight;
        } else if (chain == 'any') {
            if (height in publicByHeight)
                return true
            if (height in privateByHeight)
                return true;
            return false;
        }
        throw new Error('invalid chain specified: ' + chain);
    }

    function removeFromChain(blockMeta, chain, doThrowError) {

        if (typeof doThrowError == 'undefined') doThrowError = true;

        if (!(blockMeta.height in allHeights)) {
            throwError('Removing block that is not in chain', doThrowError);
        }

        var byHeight = null;
        var byHash = null;
        var otherByHeight = null;

        if (chain == 'private') {
            byHeight = privateByHeight;
            byHash = privateByHash;
            otherByHeight = publicByHeight;
        } else if (chain == 'public') {
            byHeight = publicByHeight;
            byHash = publicByHash;
            otherByHeight = privateByHeight;
        } else {
            throw new Error('Invalid chain identifier specified: ' + chain);
        }

        if (!(blockMeta.height in byHeight))
            throwError(chain + ' chain does not contain block', doThrowError);
        if (!(blockMeta.hash in byHash))
            throwError(chain + ' chain does not contain block', doThrowError);

        delete byHeight[blockMeta.height];
        delete byHash[blockMeta.hash];

        //remove from allHeights if needed
        if (!(blockMeta.height in otherByHeight))
            delete allHeights[blockMeta.height];

        if (Object.keys(byHeight).length == 0) {
            logger.log('warning ' + chain + ' chain is empty!');
        }

    }

    function throwError(message, doThrow) {
        if (doThrow)
            throw new Error(message);
    }

    function getMissingHashes(startHeight, endHeight, chain) {

        var heights = [];
        for (var i = startHeight; i <= endHeight; i++) {
            heights.push(i);
        }
        if (hasBeenMarkedSafe) {
            if (!smPeers.sendGetHeaders(chain)) {
                getHeightsFromRPC(heights, chain);
            }
        } else {
            getHeightsFromRPC(heights, chain);
        }
    }

    var requestedHeightsPrivate = {};
    var requestedHeightsPublic = {};

    function hasBeenRequested(height, chain) {
        if (chain == 'public') {
            if (height in requestedHeightsPublic) {
                return true;
            }
            requestedHeightsPublic[height] = 0;
        } else if (chain == 'private') {
            if (height in requestedHeightsPrivate) {
                return true;
            }
            requestedHeightsPrivate[height] = 0;
        }
        return false;
    }

    setInterval(function () {
        requestedHeightsPrivate = {};
        requestedHeightsPublic = {};
    }, 5000);

    function getHeightsFromRPC(heights, chain) {

        var daemon;
        if (chain == 'public')
            daemon = pool.public_daemon;
        else if (chain == 'private')
            daemon = pool.private_daemon;
        else
            throw new Error('invalid chain side: ' + side);

        var commands = [];

        for (var height of heights) {
            if (!hasBeenRequested(height, chain))
                commands.push(["getblockhash", [height]]);
        }

        if (hasBeenMarkedSafe && numRPCBatchesComplete < 2) {
            var n = 0;
        }

        daemon.batchCmd(commands, function (error, results) {
            if (error) {
                logger.log('error getting missing blocks: ' + error.message);
            }
            if (results == null)
                var x = daemon;
            for (var i = 0; i < results.length; i++) {

                if (results[i].error) {
                    //probably just a block height out of range error.
                    continue;
                }

                var hash = results[i].result;
                var height = commands[i][1][0];
                try {
                    var meta = new BlockMeta('rpc', chain, hash, height, null, true);
                    addToChain(meta, chain);
                } catch (err) {
                    //do nothing
                }
            }
            numRPCBatchesComplete++;

        });
    }

    var checksInProgress = {};
    var hashCheckScheduleDelays = [0, 250, 250, 500, 750, 1000, 2000, 4000, 8000];

    /**
     * Sends a delayed rpc request to check if a block message resulted in the other chain accepting the block
     */
    function scheduleHashCheck(blockMeta, chain, numChecksDone = 0, noRepeat = false, forRequestId = 0) {

        var height = blockMeta.height;
        var expectedHash = blockMeta.hash;

        var checksKey = blockMeta.height + "_" + chain + '_' + expectedHash;
        var inProgress = null;
        var isKeyFound = false;

        if (checksKey in checksInProgress) {
            inProgress = checksInProgress[checksKey];
            inProgress.numChecksDone = numChecksDone;
            isKeyFound = true;
        } else {
            inProgress = {
                numChecksDone: numChecksDone, //this is read only for debugging, the real value is passed as method param
                timerIds: [],
                height: height,
                expectedHash: expectedHash,
                requestId: 0 //used to check if a new request has been made.
            };
            checksInProgress[checksKey] = inProgress;
        }
        if (numChecksDone == 0) {
            //this is a new request for a block we are already checking.

            if (isKeyFound) {
                //a previous request is in progress for this block
                //increment the requestId so that any async call in progress can tell that a new request has
                //been made when it calls back and abort itself.
                inProgress.requestId = inProgress.requestId + 1;
            }
            //cancel all timer tasks as they will be from the old request.  This means the timer gets reset
            try {
                for (var timerId of inProgress.timerIds) {
                    clearTimeout(timerId);
                }
            } catch (e) {
                var x = 0;
            }
            inProgress.timerIds = [];
            //reset the check counter
            inProgress.numChecksDone = 0;

            //set forRequestId. Whether this is equal to currentRequestId is what determines if a request
            //will get aborted after the time interval.  Since this is the first call forRequestId hasn't been set
            // to match the current requestId.
            forRequestId = inProgress.requestId;
        }
        var currentRequestId = inProgress.requestId;
        var delay = hashCheckScheduleDelays[numChecksDone];
        if (currentRequestId != 0)
            delay += 250;

        if (numChecksDone > 6) {
            logger.log("aborting hash check for '" + chain + "' at height: " + height + " checksDone: " + numChecksDone);
            delete checksInProgress[checksKey]
            return;
        } else {
            logger.log("scheduling hash check for '" + chain + "' at height: " + height + " in " + delay + "ms - checksDone: " + numChecksDone + ' expecting: ' + expectedHash);
        }

        var timerId = setTimeout(function() {

            // if (numChecksDone > 0) {
            //     if (checksInProgress[checksKey]['numChecksDone'] === 0) {
            //         logger.log("reseting hash check for '" + chain + "' at height: " + height + " checksDone: " + numChecksDone);
            //         numChecksDone = 0;
            //     }
            // }

            //check if the requestId still matches
            var currentInProgress = checksInProgress[checksKey];
            if (!currentInProgress) {
                logger.log('checkInProgress has been deleted by another request');
                return;
            }
            if (currentInProgress.requestId !== forRequestId) {
                logger.log('rpc requestId has been updated for key[' + checksKey + '] aborting.  forRequestId: ' + forRequestId + ' currentInProgress.requestId: ' + currentInProgress.requestId);
                return;
            }


            var daemon = chain == 'public' ? pool.public_daemon : pool.private_daemon;

            var getBlockHashByRpc = function() {
                daemon.cmd('getblockhash', [height], function(response) {

                    var error = response[0].error;
                    if (error) {
                        //Block height out of range, the node hasn't processed it yet so schedule
                        //another check but give up after 10.
                        if (numChecksDone < 10 && !noRepeat)
                            scheduleHashCheck(blockMeta, chain, numChecksDone + 1, false, currentRequestId);

                        if (error.code != -8) { //block height out of range
                            logger.log("Hash check RPC error: " + JSON.stringify(error));
                        } else {
                            logger.log("Block height out of range for: " + checksKey);
                        }

                    } else {

                        var newHash = response[0].response;
                        //update the new hash
                        _this.updateHashToValidated(newHash, chain, blockMeta);
                        if (expectedHash != newHash && !noRepeat) {
                            //schedule next hash check
                            logger.log("Not accepted hash '" + chain + "' at height: " + height + " : " + newHash);
                            scheduleHashCheck(blockMeta, chain, numChecksDone + 1, true, currentRequestId);
                        } else {
                            logger.log("Updated hash '" + chain + "' at height: " + height + " : " + newHash);
                            logger.log(asShortString());

                            //remove the inProgress record if the requestId is the same.  If it is higher
                            //then another request has been made so leave it.
                            inProgress = checksInProgress[checksKey];
                            if (inProgress && inProgress.requestId == currentRequestId) {
                                delete checksInProgress[checksKey];
                            }

                        }
                        //logger.log(asShortString());

                    }
                });
            }

            if (!height || height  < 0) {
                daemon.cmd('getblock', [expectedHash], function(response) {

                    if (response[0].error) {
                        logger.log(JSON.stringify(response[0].error));
                    } else {
                        height = response[0].response.height;
                        getBlockHashByRpc();
                    }
                });
            } else {
                getBlockHashByRpc();
            }
        }, delay);
        inProgress.timerIds.push(timerId);
    }

    function isConnectable(map, blockMeta) {

        if (!equalised)
            return true;

        var ends = getEnds(map);
        if (ends == null || ends.tip == null)
            return false;
        if (blockMeta.height <= ends.tip.height + 1 && blockMeta.height >= ends.base.height - 1) {
            return true;
        }
        if (ends.tip.height < blockMeta.height) {
            //missing blocks between tip and this block so get the missing ones
            getMissingHashes(ends.tip.height + 1, blockMeta.height - 1, map == privateByHeight ? 'private' : 'public');
        }
        return false;
    }

    this.getBlockHeight = function (hash, chain) {
        var byHash = null;
        if (chain == 'public') {
            byHash = publicByHash
        } else if (chain == 'private') {
            byHash = privateByHash;
        }
        if (hash in byHash) {
            return byHash[hash].height;
        }
        return -1;
    }

    function getTip(map) {
        var ends = getEnds(map);
        return ends.tip;
    }

    function getBase(map) {
        var ends = getEnds(map);
        return ends.base;
    }

    this.getBaseHeight = function() {
        var priv = getBase(privateByHeight);
        var pub = getBase([publicByHeight]);
        var height = null;
        if (priv)
            height = priv.height;
        if (pub && height !== null && pub.height < height)
            height = pub.height;
        return height;
    }

    function getEnds(map) {
        var keys = getSortedKeys(map);
        var tip = keys.length > 0 ? map[keys[keys.length - 1]] : null;
        var base = keys.length > 0 ? map[keys[0]] : null;
        return {'base': base, 'tip': tip};
    }

    function connectUnconnected(chain) {
        var map = chain == 'private' ? privateUnconnected : publicUnconnected;
        var ends = chain == 'private' ? getEnds(privateByHeight) : getEnds(publicByHeight);
        var sorted = getSortedMap(map);
        for (var index in sorted) {
            if (isInChain(index, chain)) {
                //already connected
                delete map[index];
            } else if (ends && ends.base && map[index].height < ends.base.height) {
                //block is from before the chain segment we are recording so get rid of it.
                delete map[index];

            } else if (addToChain(map[index], chain, true))
                delete map[index];

        }
    }

    function addToChain(blockMeta, chain, noConnect = false) {

        var byHeight;
        var byHash;
        var unconnected;
        var otherByHash;
        var otherByHeight;

        if (chain == 'private') {
            byHeight = privateByHeight;
            byHash = privateByHash;
            unconnected = privateUnconnected;
            otherByHash = publicByHash;
            otherByHeight = publicByHeight;
        } else if (chain == 'public') {
            byHeight = publicByHeight;
            byHash = publicByHash;
            unconnected = publicUnconnected;
            otherByHash = privateByHash;
            otherByHeight = privateByHeight;
        } else {
            throw new Error('Invalid chain identifier specified: ' + chain);
        }

        var otherChainMetaByHeight = null;

        if (blockMeta.height in byHeight) {
            var originalMetaByHeight = byHeight[blockMeta.height];
        }

        if (!blockMeta.validated) {
            //check we don't already have the block in a validated state, if so change this meta to validated
            //this is only likely to happen at startup as we get a bunch of blocks via rpc from both sides and
            //might get a headers message from the other side after.  If that happens we need to do this check
            //or a block might be incorrectly marked unvalidated.
            if (originalMetaByHeight) {
                if (originalMetaByHeight.hash == blockMeta.hash && originalMetaByHeight.validated) {
                    blockMeta.validated = true;
                }
            }
        }
        //check again
        if (!blockMeta.validated) {
            //non validated blocks are one's that have been received from the other chain
            //we need to schedule an rpc check to find out if the node accepts the block
            scheduleHashCheck(blockMeta,chain);
        }

        if (isConnectable(byHeight, blockMeta)) {
            byHeight[blockMeta.height] = blockMeta;
            //byHeight = getSortedMap(byHeight);
            byHash[blockMeta.hash] = blockMeta;
            allHeights[blockMeta.height] = 0;

            //check byHash for any other blocks at same height with different hash
            //this would indicate a reorg has happened as the daemon has sent us a different version
            //of this block height previously

            for (var otherHash in byHash) {
                var otherMeta = byHash[otherHash];
                if (otherMeta.height == blockMeta.height && otherHash != blockMeta.hash) {
                    //reorg has happened - delete the old hash
                    delete byHash[otherHash];
                }
            }

            //if this block doesn't have a message and a message exists in the other chain then add it.
            if (!blockMeta.hasOwnProperty('message') || !blockMeta.message) {
                if (blockMeta.hash in otherByHash) {
                    var otherChainMeta = otherByHash[blockMeta.hash];
                    //just another sanity check
                    if (otherChainMeta.hash != blockMeta.hash)
                        throw new Error("hash key doesn't match hash property");

                    if (otherChainMeta.hasOwnProperty('message')) {
                        blockMeta.message = otherChainMeta.message;
                    }
                }
            }


            if (!noConnect)
                connectUnconnected(chain);
            if (checkEqualised())
                trimToCommonBase();
            if (trimmedToCommonBase)
                pruneChainBase();
            return true
        } else {
            unconnected[blockMeta.height] = blockMeta;
            return false;
        }
    }

    function isContiguous(map) {
        var sorted = getSortedKeys(map);
        var lastIndex = null;
        var missing = [];
        for (var index of sorted) {
            var index = parseInt(index);
            if (lastIndex == null) {
                lastIndex = index;
                continue;
            }
            if (index != lastIndex + 1) {
                //there's a gap so schedule an rpc request to fill it.
                for (var i = lastIndex + 1; i < index; i++)
                    missing.push(i);
            }
            lastIndex = index;
        }
        if (missing.length == 0) {
            return true;
        } else {
            getHeightsFromRPC(missing, map == privateByHeight ? 'private' : 'public');
            return false;
        }

    }

    function getSortedMap(map) {
        var keys = getSortedKeys(map);
        var sorted = {};
        for (var key of keys) {
            sorted[key] = map[key];
        }
        return map;
    }

    function getSortedKeys(map) {
        var keys = Object.keys(map).slice();
        keys.sort(function (a, b) {
            return a - b;
        })
        return keys;
    }

    this.updateHashToValidated = function (hash, chain, blockMeta) {
        var message = smfunc.getCachedHeadersMessage(hash);
        if (blockMeta) {
            blockMeta = blockMeta.clone();
            if (message) {
                blockMeta.message = message;
                blockMeta.prevHash = message.prevHash;
            } else {
                if (trimmedToCommonBase)
                    var x = 0;
                blockMeta.message = null;
                blockMeta.prevHash = null;
            }
            blockMeta.hash = hash;
            //blockMeta.chain = chain; this causes problems ???
            blockMeta.validated = true;
            //this will make sure pruning and everything else that matters happens to update the chain state
            addToChain(blockMeta, chain);
        }
    }

    this.getBlocksForSuppression = function () {

        var state = validateState();
        var suppressed = [];

        for (var height in publicByHeight) {
            var meta = publicByHeight[height];
            if (meta.matchStatus == '!=') {
                suppressed.push(meta);
            }
        }
        return suppressed;
    }

    /**
     * Adds a public block to the smManagers to internal representation of the chain.  This is usually used when
     * forwarding a block received from the opposite chain. As such we have no garuntee whether the block will be accepted.
     *
     * @param hash
     * @param height
     * @param validated whether we are certain this block is the one the peer has accepted. Usually this means that
     * we learned about this block via a p2p headers message direct from the peer.  In most cases when calling this method
     * if will be because we are sending to block header to the public peer so validated will be false.
     * @param message
     * @param log
     * @return {*}
     */
    this.addPublicBlock = function (hash, height, validated = false, message = null, log = true) {
        return _addBlock(hash, height, message, 'public', validated, log);
    }

    /**
     * Adds a private block to the smManagers to internal representation of the chain.  This is usually used when
     * forwarding a block received from the opposite chain. As such we have no garuntee whether the block will be accepted.
     *
     * @param hash
     * @param height
     * @param validated whether we are certain this block is the one the peer has accepted. Usually this means that
     * we learned about this block via a p2p headers message direct from the peer.  In most cases when calling this method
     * if will be because we are sending to block header to the private peer so validated will be false.
     * @param message
     * @param log
     * @return {*}
     */
    this.addPrivateBlock = function (hash, height, validated = false, message = null, log = true) {
        return _addBlock(hash, height, message, 'private', validated, log);
    }

    function _addBlock(hash, height, message, chain, validated, log) {
        var meta = new BlockMeta('addBlock', chain, hash, height, message, validated);

        var result = addToChain(meta, chain);
        if (log)
            logger.log(asShortString());
        return result;
    }

    /**
     * When we receive a headers message from the private peer we assume the private pool has mined a new block.  After
     * to adding the block to the internal representation of the branches, the SM algorithm is run and a list blocks
     * that should be published on both the private and public chains is returned.
     *
     * @param hash
     * @param height
     * @param message
     * @param log
     * @return {*}
     */
    this.handlePrivateMinedBlock = function (hash, height, message, log = true) {
        return _handleMinedBlock('private', hash, height, message, log);
    };

    /**
     * When we receive a headers message from the private peer we assume the private pool has mined a new block.  After
     * to adding the block to the internal representation of the branches, the SM algorithm is run and a list blocks
     * that should be published on both the private and public chains is returned.
     *
     * @param hash
     * @param height
     * @param message
     * @param log
     * @return {*}
     */
    this.handlePublicMinedBlock = function (hash, height, message, log = true) {
        return _handleMinedBlock('public', hash, height, message, log);
    }

    function _handleMinedBlock(chain, hash, height, message, log) {

        //debug
        if (chain != message.receivedBy)
            throw new Error('mismatched chain and receivedBy')

        if (log)
            logger.log("before add - " + asShortString());

        var priorPrivateLead = getPrivateBranchLead();

        logger.log("Received " + chain + " block [" + height + "]: " + hash + (message ? " - '" + message.command + "' message attached" : " - no message attached"));
        var meta = new BlockMeta('handleMinedBlock', chain, hash, height, message, true);
        var result = addToChain(meta, chain);

        if (log)
            logger.log("after add - " + asShortString());

        //confirm bases are matching
        if (trimmedToCommonBase) {
            var privateBase = getBase(privateByHeight);
            var publicBase = getBase(publicByHeight);
            if (privateBase && publicBase) {
                if (privateBase.hash != publicBase.hash) {
                    //not matching so schedule a hash check to make sure we are on the correct block on both sides
                    scheduleHashCheck(privateBase, 'private');
                    scheduleHashCheck(publicBase, 'public');
                }
            }
        }
        var baseHeight = _this.getBaseHeight();
        if (baseHeight && baseHeight != height) {
            //schedule a hash check for this height also
            scheduleHashCheck(meta, chain);
            //and the other chain
            var otherByHeight = chain == 'public' ? publicByHeight : privateByHeight;
            var otherMeta = height in otherByHeight ? otherByHeight[height] : null;
            if (otherMeta) {
                scheduleHashCheck(otherMeta, chain == 'public' ? 'private' : 'public');
            }
        }

        // get a list of blocks to send to the other side.

        var relayBlocks = null;
        var otherChain = null;

        if (chain == 'private') {
            relayBlocks = getBlocksToBroadCastForPrivateMinedBlock(meta)
            otherChain = 'public';
        } else if (chain = 'public') {
            relayBlocks = getBlocksToBroadCastForPublicMinedBlock(meta);
            otherChain = 'private'
        } else {
            throw new Error('chain not specified.')
        }

        if (!relayBlocks)
            throw new Error('no relay blocks array returned');

        //get heights for logging
        var relayedHeights = {
            'public': [],
            'private': []
        };
        for (var chain in relayBlocks) {
            for (var relayMeta of relayBlocks[chain])
                relayedHeights[chain].push(relayMeta.height);
        }

        logger.log('pushing blocks to: ' + JSON.stringify(relayedHeights));
        return relayBlocks;
    }

    /**
     * @return {*} integer length of private branch lead or false if can't be determined reliably.
     */
    function getPrivateBranchLead() {
        var privateBase = getBase(privateByHeight);
        var publicBase = getBase(publicByHeight);
        if (!privateBase || !publicBase)
            return false;
        if (privateBase.height != publicBase.height)
            return false;
        var privateBranchLen = Object.keys(privateByHeight).length;
        var publicBranchLen = Object.keys(publicByHeight).length;
        return privateBranchLen - publicBranchLen;
    }

    /**
     * This is the main SM function that determines whether private mined blocks should be sent to public peers.
     *
     * We are following Emin's algorithm from page 6 of https://www.cs.cornell.edu/~ie53/publications/btcProcFC.pdf
     *
     * @return A list of blocks to send to the private and public sides of the network
     *
     */
    function getBlocksToBroadCastForPrivateMinedBlock(privBlockMeta) {

        var state = getShortState(); //for easier debug

        var relayBlocks = {
            'private': [],
            'public': []
        }

        if (!trimmedToCommonBase) {
            //not ready to test validState yet so just relay the block to the other side like a good citizen.
            relayBlocks.public.push(privBlockMeta);
            return relayBlocks;
        }

        var validState = validateState();
        if (!validState.isSafeToMine) {
            //just relay the message as is since we are not in SM mode
            relayBlocks.public.push(privBlockMeta);
            return _checkMetas(relayBlocks);
        }
        // -1 because the map includes the most recent common block which isn't part of a branch
        // -unvalidatedLength because we may have some blocks where the peer may have seen the header but not the
        // block itself.  We keep them in our internal pruned chain but we are treating the first block where both
        // are valid and matching as the start of the branch
        var privateBranchLen = Object.keys(privateByHeight).length - 1 - unvalidatedLength;
        var publicBranchLen = Object.keys(publicByHeight).length - 1 - unvalidatedLength;
        //deltaPrev is the difference between branch lengths prior to the new private block being mined
        var deltaPrev = privateBranchLen - publicBranchLen - 1; // -1 because we've already added the private block at this point

        logger.log('check private block - privateBranchLen: ' + privateBranchLen + " publicBranchLen: " + publicBranchLen + " deltaPrev: " + deltaPrev + " unvalidatedLength: " + unvalidatedLength);


        if (deltaPrev == 0 && privateBranchLen >= 2) {
            //publish all of private chain
            relayBlocks = _addAllFromChain('private', relayBlocks);
            return _checkMetas(relayBlocks);
        }
        //next step in algorithm is 'Mine at the head of the private chain'.  no need to do anything here
        //as by this stage we already are.
        return _checkMetas(relayBlocks);
    }

    /**
     * This is the main SM function that determines whether public mined blocks should be passed to the private peer
     * or whether a public mined block should trigger release of private blocks.
     *
     * We are following Emin's algorithm from page 6 of https://www.cs.cornell.edu/~ie53/publications/btcProcFC.pdf
     *
     * @return A list of blocks to send to the private and public sides of the network
     */
    function getBlocksToBroadCastForPublicMinedBlock(pubBlockMeta) {

        var state = getShortState(); //for easier debug

        var relayBlocks = {
            'private': [],
            'public': []
        }

        if (!trimmedToCommonBase) {
            //not ready to test validState yet so just relay the block to the other side like a good citizen.
            relayBlocks.private.push(pubBlockMeta);
            return relayBlocks;
        }

        var validState = validateState(false);
        if (!validState.isSafeToMine) {
            //just relay the message as is since we are not in SM mode
            relayBlocks.private.push(pubBlockMeta);
            return _checkMetas(relayBlocks);
        }
        // -1 because the map includes the most recent common block which isn't part of a branch
        // -unvalidatedLength because we may have some blocks where the peer may have seen the header but not the
        // block itself.  We keep them in our internal pruned chain but we are treating the first block where both
        // are valid and matching as the start of the branch
        var privateBranchLen = Object.keys(privateByHeight).length - 1 - unvalidatedLength;
        var publicBranchLen = Object.keys(publicByHeight).length - 1 - unvalidatedLength;

        //deltaPrev is the difference between branch lengths prior to the new block being mined
        // -1 because we've already added the public block at this point
        var deltaPrev = privateBranchLen - (publicBranchLen - 1);

        logger.log('check private block - privateBranchLen: ' + privateBranchLen + " publicBranchLen: " + publicBranchLen + " deltaPrev: " + deltaPrev + " unvalidatedLength: " + unvalidatedLength);

        if (deltaPrev < 0) {
            //we are behind and probably catching up to the public chain so just relay the block
            relayBlocks.private.push(pubBlockMeta);
            return _checkMetas(relayBlocks);
        }

        if (deltaPrev == 0) {
            //they win - relay the block to private side
            //send all blocks as we may have a chains length greater than 1.  This happens when the private
            //side has previously mined one block ahead then public mines an equal block height block.  If the public
            //side doesn't accept the private block the next block will be on top of the previous public block which
            //is unseen by the private node.  As such the chain is broken and the private node will still think it's
            //fork is the only valid one.
            //relayBlocks.private.push(pubBlockMeta);
            relayBlocks = _addAllFromChain('public', relayBlocks);
            return _checkMetas(relayBlocks);
        }
        if (deltaPrev == 1) {
            //now same length, try our luck
            //publish our private block
            var tip = getTip(privateByHeight)
            relayBlocks.public.push(tip);
            logOneBlockLeadRace(tip)
            return _checkMetas(relayBlocks);
        }
        if (deltaPrev == 2) {
            //we are ahead by 2 and will win so publish all of private chain
            // var skip = true;
            // var numSkipped = 0;
            // for (var height in privateByHeight) {
            //     if (numSkipped < 1 + unvalidatedLength) {
            //         skip = false;
            //         numSkipped++;
            //         continue;
            //     }
            //     var blockMeta = privateByHeight[height];
            //     if (!blockMeta)
            //         throw new Error('Missing blockMeta');
            //     if (!blockMeta.hasOwnProperty('message') || !blockMeta.message)
            //         throw new Error('blockMeta is missing wire message');
            //     relayBlocks.public.push(blockMeta);
            //
            // }
            relayBlocks = _addAllFromChain('private', relayBlocks);
            return _checkMetas(relayBlocks);
        }
        //we have a private chain longer than two so just publish the first unpublished block in our chain
        //this maintains our lead.
        //for simplicity we will publish all blocks we've got in our chain except the last two.
        //we are only publishing header messages so the other peer will ignore them if they've already seen
        //the block.
        relayBlocks = _addAllFromChain('private', relayBlocks, 2);

        return _checkMetas(relayBlocks);
    }

    /**
     * Logs block height and hash of any blocks where the selfish miner had a one block lead and the public
     * side found a block.  This leads to a race to propogate the block as fast as possible to get lambda
     * as close to one as possible.  We'll use this log file later to see how many of these races are actually won.
     * @param blockMeta
     */
    function logOneBlockLeadRace(blockMeta) {
        blockMeta = blockMeta.clone();
        var file = '../one_block_leads.json';
        var json = {
            height: blockMeta.height,
            hash: blockMeta.hash
        }
        var json = JSON.stringify(json) + "\n";
        fs.appendFile(file, json, null, function () {});

        //schedule a check in ten seconds time to count how many nodes are mining on top of this block
        // setTimeout(function() {
        //     var daemon = process.is_dev ? pool.public_daemon : pool.other_daemons;
        //     var commands = [["getinfo"], ["getblockhash", [blockMeta.height]]];
        //     daemon.cmd("getinfo", [], function(results, error) {
        //
        //         if (error) {
        //             logger.log('RPC error checking blockheight')
        //         } else {
        //             //if height is greater this test is invalid so abort
        //
        //             var maxHeight = 0;
        //
        //             for (var i = 0; i < results.length; i++) {
        //                 if (results[i].error)
        //                     continue;
        //                 if (results[i].response && results[i].response.blocks > maxHeight) {
        //                     maxHeight = results[i].response.blocks;
        //                 }
        //             }
        //
        //             daemon.cmd("getblockhash", [blockMeta.height], function(results, error) {
        //                 if (error)
        //                     logger.log('RPC error checking blockhash')
        //
        //                 var count = results.length;
        //                 var equalHashes = 0;
        //                 var hasError = false;
        //                 var otherHashes = {}
        //
        //                 for (var i = 0; i < results.length; i++) {
        //                     if (results[i].error) {
        //                         hasError = true;
        //                         continue;
        //                     }
        //                     var otherHash = results[i].response;
        //                     if (otherHash == blockMeta.hash) {
        //                         equalHashes++;
        //                     } else {
        //                         otherHashes[otherHash] = null;
        //                     }
        //                 }
        //
        //                 var lambda = parseFloat(equalHashes) / count;
        //                 var valid = !(maxHeight == 0 || maxHeight > blockMeta.height)
        //
        //                 var out = {
        //                     height: blockMeta.height,
        //                     hash: blockMeta.hash,
        //                     miningOnSmBlock: equalHashes,
        //                     miningOnPublicBlock: count - equalHashes,
        //                     totalNodes: count,
        //                     lambda: lambda,
        //                     valid: valid,
        //                     maxHeightSeen: maxHeight,
        //                     otherHashes: Object.keys(otherHashes)
        //                 }
        //                 logger.log("block " + out.height + " [" + out.hash + "] " + out.miningOnSmBlock + "/" + out.totalNodes + " lambda: " + out.lambda + " valid: " + out.valid + " otherHashes: " + JSON.stringify(out.otherHashes));
        //                 out = JSON.stringify(out) + "\n";
        //                 fs.appendFile('../lambdas.json', out, null, function(){});
        //
        //             })
        //
        //         }
        //     })
        // }, 10000);
    }

    /**
     *
     * @param fromChain
     * @param relayBlocks
     * @param skipLast how many blocks at head of chain to skip.
     * @return {*}
     * @private
     */
    function _addAllFromChain(fromChain, relayBlocks, skipLast = 0) {

        var toChain = fromChain == 'public' ? 'private' : 'public';
        var fromByHeight = fromChain == 'public' ? publicByHeight : privateByHeight;

        var skip = true;
        var numSkipped = 0;
        var len = Object.keys(fromByHeight).length;
        var numProcessed = 0;
        for (var height in fromByHeight) {
            numProcessed++;
            if (numProcessed > len - skipLast) {
                break;
            }
            if (numSkipped < 1 + unvalidatedLength) {
                skip = false;
                numSkipped++;
                continue;
            }
            var blockMeta = fromByHeight[height];
            if (!blockMeta)
                throw new Error('Missing blockMeta');
            if (!blockMeta.hasOwnProperty('message') || !blockMeta.message)
                throw new Error('blockMeta is missing wire message');
            relayBlocks[toChain].push(blockMeta)

        }
        return relayBlocks;
    }

    /**
     * Sanity check. Confirm all metas have a message and throw error if not.
     * @param metas
     * @return {*}
     * @private
     */
    function _checkMetas(metas) {
        var heights = {};
        for (var chain in metas) {
            for (var meta of metas[chain]) {
                if (!meta.hasOwnProperty('message') || !meta.message)
                    throw new Error('blockMeta is missing wire message');
                if (isNaN(meta.height) || meta.height == null)
                    throw new Error('blockMeta missing height');
                if (meta.height in heights)
                    throw new Error('duplicate heights in chain: ' + chain);
                heights[meta.height] = 0;
            }
        }
        return metas;
    }


    function getState() {
        var obj = {
            'equalised': equalised,
            'trimmedToCommonBase': trimmedToCommonBase,
            'privateChain': privateByHeight,
            'publicChain': publicByHeight,
            'privateUnconnected': getSortedMap(privateUnconnected),
            'publicUnconnected': getSortedMap(publicUnconnected),
            'validState': validateState(false)
        }
        return obj;
    }

    function getShortState() {
        var validState = validateState(false);

        if (validState.isSafeToMine) {
            var obj = {
                'privateChain': Object.keys(privateByHeight),
                'publicChain': Object.keys(publicByHeight),
                'selfishMining': true
            }
            return _addMarkersToStateHeights(obj);


        } else {

            var obj = {
                'equalised': equalised,
                'trimmedToCommonBase': trimmedToCommonBase,
                'privateChain': Object.keys(privateByHeight),
                'publicChain': Object.keys(publicByHeight),
                'privateUnconnected': Object.keys(getSortedMap(privateUnconnected)),
                'publicUnconnected': Object.keys(getSortedMap(publicUnconnected)),
                'validState': validState
            }
            return _addMarkersToStateHeights(obj);
        }
    }

    /**
     * adds mnemonic markers to heights for easier diagnostics in the log.
     * @param obj
     * @return {*}
     * @private
     */
    function _addMarkersToStateHeights(obj) {
        obj.privateChain.forEach(function (height, index, arr) {
            var meta = privateByHeight[height];
            var hashPart = meta.hash.slice(-2);
            var hasMessage = meta.message ? 'm' : 'x';
            arr[index] = hasMessage + meta.matchStatus + height + (meta.validated ? 'v-' : 'x-') + hashPart;
        });

        obj.publicChain.forEach(function (height, index, arr) {
            var meta = publicByHeight[height];
            var hashPart = meta.hash.slice(-2);
            var hasMessage = meta.message ? 'm' : 'x';
            arr[index] = hasMessage + meta.matchStatus + height + (meta.validated ? 'v-' : 'x-') + hashPart;

            //add a space to the frist entry so it lines up with the private side in the logs
            if (index == 0)
                arr[index] = ' ' + arr[index];

        });
        return obj;
    }

    function asString() {

        return JSON.stringify(getState(), null, 4);
    }


    /**
     * Gets a text representation of the state of the internal private and public chains.
     * The chain representations are a little cryptic.
     *
     *      "privateChain": "[m==10852v-8f,m--10853v-51,m??10854v-51]",
     *      "publicChain": "[ x==10852v-8f]",
     *
     * The first char is either:
     * m: blockMeta contains a message object
     * x: blockMeta is missing a message object
     *
     * In the above example the first chars represent the match state bewteen the two chains at that block height
     * == : matching block hashes
     * != : mismated block hashes
     * -- : the other chain doesn't have a block at this height.
     *
     * The number immediately following is the block height
     *
     * The next char is either
     * v : validated - that is that we know the peer on this side has accepted this block hash
     * x: unvalidated - we don't know that the peer on this side has accepted this block hash but it may have.
     *
     * The final two chars following the '-' are the last hex byte of the block hash to make it easier to match it to
     * other parts of the log where block height and full hashes are displayed.
     *
     */
    function asShortString() {
        var state = getShortState();
        return JSON.stringify(state, function (k, v) {
            if (v instanceof Array)
                return JSON.stringify(v).split('"').join("");
            return v;
        }, 4);
    }

    this.logShortString = function () {
        logger.log(asShortString());
    }


}
