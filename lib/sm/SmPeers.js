/**
 * Created by shadders on 19/07/17.
 */

var util = require('util');
var fs = require('fs');

var smFunctions = require('./smFunctions');

var p2p = require('bitcore-p2p')
var bitcore = require('bitcore-lib');
var BufferWriter = bitcore.encoding.BufferWriter;
var Buffers = bitcore.encoding.Buffers;
var Networks = bitcore.Networks;
Networks.enableRegtest();
Networks.defaultNetwork = Networks.testnet;
var Peer = p2p.Peer;
var Messages = p2p.Messages;
var messages = new Messages();
var _messages = messages;
var BufferUtil = bitcore.util.buffer;

var SMPeers = module.exports = function (pool, smManager, logger, privateStartHeight, _smFunctions) {

    var _this = this;
    var smfunc = _smFunctions;

    var masterPeerParams = JSON.parse(fs.readFileSync(process.sm_config_dir + 'peers-master.json'));
    var otherPeerParams = JSON.parse(fs.readFileSync(process.sm_config_dir + 'peers-other.json'));

    var peerPrivateParams = masterPeerParams['private'];
    var peerPublicParams = masterPeerParams['public'];

    for (var i = 0; i < otherPeerParams.length; i++) {
        otherPeerParams[i]['id'] = i;
    }

    var peerPrivate;
    var peerPublic;
    var otherPeers = {};
    //when sending out messages to other peers in a block race we want to go in reverse order since
    //the selfish mining nodes are at the beginning of the list.  We want to honest mining nodes to see
    //the message as soon as possible.
    var otherPeersReverseKeys = [];
    var allPeers = {};

    var smManagerChainInitialized = false;

    // moved to smFunctions.js
    // var seenHashes = {};

    var heighestSeenHeight = 0;

    var lastGetHeadersPrivate = null;
    var lastGetHeadersPublic = null;

    this.sendGetHeaders = function(chain) {
        var peer = chain == 'public' ? peerPublic : peerPrivate;
        var message = chain == 'public' ? lastGetHeadersPublic : lastGetHeadersPrivate;
        if (message != null) {
            maybeSendMessage(peer, message, false, true);
            return true;
        }
        return false;
    };

    //register of inv messages we've seen from public and requested a headers message for.
    var requestedInvs = {};

    function registerRequestedInvEntry(hash) {
        var entry = null;
        if (hash in requestedInvs) {
            entry = requestedInvs[hash];
        } else {
            entry = {
                'modTime': (new Date()).getTime(),
                'hash': hash,
                'getheadersMessage': null,
                'sentToAll': false
            }
            requestedInvs[hash] = entry;
        }

    }

    function getRequestedInvEntry(hash) {
        if (isRequestedInv(hash)) {
            return requestedInvs[hash];
        }
        return null;
    }

    function isRequestedInv(hash) {
        return hash in requestedInvs;
    }

    //a store of block header hashes relayed to private peer to make sure we don't double up
    var relayedToPrivate = {};

    function registerRelayedToPrivateEntry(message) {
        var hash = getHashForMessage(message);
        var entry = null;
        if (hash in relayedToPrivate) {
            entry = relayedToPrivate[hash];
        } else {
            entry = {
                'modTime': (new Date()).getTime(),
                'hash': hash
            }
            relayedToPrivate[hash] = entry;
        }
        entry[message.command] = true;
    }

    function isRelayedToPrivate(message) {
        if (message.command == 'getheaders') {
            //getheaders commands will frequently look similar ones so let them through.
            return false;
        }
        var hash = getHashForMessage(message);
        if (hash in relayedToPrivate) {
            var entry = relayedToPrivate[hash];
            return message.command in entry && entry[message.command];
        }
        return false;
    }

    //A timed out cache of inv, header and block messages for recently mined private blocks
    //this allows us to simulate the message sequence with other peers and speed things up.
    //the three awaitingXxx properties are for peers to register themselves as waiting for a particular
    //message if they receive a request before we have the response from our private peer.
    //this when the message comes in we can fire it off to all who've requested it.
    var privateMessageStore = {};

    function _newPrivateMessageEntry(hash, height = null) {
        if (hash == null)
            throw new Error('missing hash');
        if (height === null && privateMessageStore)
            height = maybeGetBlockHeight(hash);
        var entry = {
            'modTime': (new Date()).getTime(),
            'hash': hash,
            'height': height,
            'inv': null,
            'headers': null,
            'block': null,
            'awaiting_inv': {},
            'awaiting_headers': {},
            'awaiting_block': {},
            'sentTo_inv': {},
            'sentTo_headers': {},
            'sentTo_block': {},
            'registered': false,
            'blockRequested': false
        }
        privateMessageStore[hash] = entry;
        return entry;
    }

    delete privateMessageStore['exampleHash_00000000000_etc'];

    //register of block hashes we consider to be competing with our own, we will not relay
    //inv or headers messages for these blocks
    var competingBlocks = {};

    function _newCompetingBlockEntry(height, hash) {
        if (!height) {
            height = maybeGetBlockHeight(hash);
        }
        var entry = {
            'hash': hash,
            'height': height,
            'modTime': (new Date()).getTime()
        }
        return entry;
    }

    /**
     * initialize the cache cleaner
     */

    function filterTimedOut(name, map, timeoutMins, checkHeights = false) {
        var timeout = timeoutMins * 60 * 1000;
        var filteredMap = {};
        var time = (new Date()).getTime();
        var total = 0;
        var trimmed = 0;
        for (var key in map) {
            var entry = map[key];
            total++;

            var forceRetain = false;
            //used to ensure we retain any entries for currently contested blocks past the timeout
            if (checkHeights) {
                var baseHeight = smManager.getBaseHeight();
                var height = entry['height'];
                if (baseHeight && height && height > baseHeight)
                    forceRetain = true;
            }

            if (time < entry.modTime + timeout || forceRetain) {
                filteredMap[key] = entry;
            } else {
                trimmed++;
            }
        }
        logger.log('Trimmed ' + trimmed + ' of ' + total + " entries from " + name);
        return filteredMap;
    }
    setInterval(function () {

        //clean out expired privateMessageStore entries
        privateMessageStore = filterTimedOut('privateMessageStore', privateMessageStore, 180, true);

        //clean out competing block entries
        competingBlocks = filterTimedOut('competingBlocks', competingBlocks, 180);

        //clean out relayed to private entries
        relayedToPrivate = filterTimedOut('relayedToPrivate', relayedToPrivate, 180);

        //clean out inv messages requested from public nodes
        requestedInvs = filterTimedOut('requestedInvs', requestedInvs, 180);

    }, 300 * 1000);

    /**
     * registers messages from private in local cache so we can respond to otherPeers immediately
     */
    function registerPrivateMessage(hash, height, message, allowBreakdown = true) {
        // if (message.receivedBy != 'private')
        //     throw new Error('attempting to register non private message');
        var command = message.command;
        if (command != 'inv' && command != 'headers' && command != 'block')
            throw new Error('illegal message type: ' + command);

        var entry;
        if (hash in privateMessageStore) {
            entry = privateMessageStore[hash];
            //update the modTime
            entry.modTime = (new Date()).getTime();
            //add the height now that we know it.
            if (entry.height === null) {
                entry.height = height;
            }

        } else {
            entry = _newPrivateMessageEntry(hash, height);
        }

        var awaitingKey = 'awaiting_' + command;
        var sentToKey = 'sentTo_' + command;

        //mark the entry as registered so we can tell that a private block actually exists for this hash
        //it's possible a public peers has just asked for it and caused a skeleton object to be in the privateMessageStore
        entry.registered = true;

        entry[command] = message;

        //send the message to any peers who registered that they are waiting for it
        var sentCount = 0;
        for (var id in entry[awaitingKey]) {
            var peer = entry[awaitingKey][id];
            if (maybeSendMessage(peer, message)) {
                entry[sentToKey][id] = peer;
                sentCount++;
                delete entry[awaitingKey][id];
            }
        }
        logger.log('Sent cached private ' + message.command + '[' + height + '] message to ' + sentCount + ' peers');

        //if the message is inv or headers and contains multiple entries create a cache entry for each one
        if (allowBreakdown) {
            if (command == 'headers') {
                for (var header of message.headers) {
                    var thisHash = reverseBufferToString(header._getHash());
                    if (thisHash != hash) {
                        var newMessage = messages.Headers();
                        //new message with only this header;
                        newMessage.headers = [header];
                        newMessage.receivedBy = message.receivedBy;
                        registerPrivateMessage(thisHash, null, newMessage, false);
                    }
                }
            } else if (command = 'inv') {
                logger.log("Error we saw an inv message");
            }
        }
    }

    function dumpPrivateMessageCacheState(prefix, hash) {

        //TODO delete this method
        return; //disable

        var cache = [];
        var entry = hash in privateMessageStore ? privateMessageStore[hash] : "[NOT FOUND]";
        var out = JSON.stringify(entry, function(key, value) {
            if (key == 'peer') {
                return 'peer[' + value.name + '] - ' + value.chain;
            }
            if (key == 'network')
                return '[network]';

            if (key == 'headers' && value)
                return 'headers: [' + value.height + '] ' + value.hash;

            if (key == 'block' && value)
                return 'block: [' + value.height + '] ' + value.hash;

            if (value && value.hasOwnProperty('dataBuffer')) //peer object
                return 'peer[' + value.name + '] - ' + value.chain;

            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    // Circular reference found, discard key
                    return '[circular]';
                }
                // Store value in our collection
                cache.push(value);
            }
            return value;
        }, 4);
        out = "privateMessageCache [" + prefix + "] : " + out;
        logger.log(out);
    }

    /**
     *
     * @param hash
     * @param height the height if you know it, will be added to the entry if missing, if you don't know it pass null
     * @param command
     * @param otherPeer
     * @param registerForDelayedMessage
     * @return {*}
     */
    function getPrivateMessage(hash, height, command, otherPeer, registerForDelayedMessage = true) {

        var entry;
        var awaitingKey = 'awaiting_' + command;


        if (!height) {
            height = maybeGetBlockHeight(hash);
        }

        if (hash in privateMessageStore) {

            entry = privateMessageStore[hash];

            //add the height if missing
            if (height && !entry.height) {
                entry.height = height;
            }

            if (entry[command]) {
                var message = entry[command];
                var id = otherPeer.id;
                if (id in entry[awaitingKey]) {
                    delete entry[awaitingKey][id];
                }
                return message;
            }

        } else {
            //create the entry so we can register for the message
            entry = _newPrivateMessageEntry(hash, height);
            privateMessageStore[hash] = entry;
        }
        //this registers the peer as waiting so it will be sent the message when it arrives.
        if (registerForDelayedMessage)
            entry[awaitingKey][otherPeer.id] = otherPeer;
        return false;
    }

    function isPrivateMessageSentTo(hash, peerId, command) {
        var entry;
        var sentToKey = 'sentTo_' + command;

        if (hash in privateMessageStore) {
            entry = privateMessageStore[hash];
            return peerId in entry[sentToKey];
        }
        return null;
    }

    function markPrivateMessageSentTo(hash, peerId, command) {
        var entry;
        var sentToKey = 'sentTo_' + command;
        var awaitingKey = 'awaiting_' + command;

        if (hash in privateMessageStore) {
            entry = privateMessageStore[hash];
            entry[sentToKey][peerId] = allPeers[peerId];
            if (peerId in entry[awaitingKey]) {
                delete entry[awaitingKey][peerId];
            }
        }
    }

    function markPrivateBlockRequested(hash, height) {
        var entry;

        if (hash in privateMessageStore) {
            entry = privateMessageStore[hash];
            entry.blockRequested = true;
        } else {
            throw new Error("private entry doesn't exist for block [" + height + "] " + hash);
        }
    }

    function getPrivateMessageHeight(hash) {
        if (hash in privateMessageStore) {
            var entry = privateMessageStore[hash];
            if (!entry.height)
                entry.height = maybeGetBlockHeight(hash);
            return entry.height;
        }
        return null;
    }

    function isPrivateMessageRegistered(hash, command) {
        if (hash in privateMessageStore) {
            var entry = privateMessageStore[hash];
            var message = entry[command];
            return message != null;
        }
        return false;
    }

    function isPrivateBlockRequestedOrPresent(hash, command) {
        if (hash in privateMessageStore) {
            var entry = privateMessageStore[hash];
            var message = entry[command];

            return entry.blockRequested || message != null;
        }
        return false;
    }

    function registerCompetingBlock(hash, height) {
        if (hash in competingBlocks) {
            //just update the mod time.
            competingBlocks[hash].modTime = (new Date()).getTime();
        } else {
            competingBlocks[hash] = _newCompetingBlockEntry(height, hash);
        }
    }

    function isCompetingBlock(hash) {
        return hash in competingBlocks;
    }

    /**
     * Initialise the smManager's internal chain state by pulling all blocks from the height of the lowest side to the
     * height of the heighest side
     */
    this.startupChainSync = function (onCompleteCallback) {

        //first we'll let the peers talk to each and sync until we match blocks
        var peers = startSimpleRelay(peerPrivateParams, peerPublicParams);
        var interval = 2000;

        var lastPrivateHeight = 0;
        var timesSeenAtHeight = 0;


        // /start checking heights of both chains until they match
        function scheduleHeightCheck(interval) {

            var timerId = setInterval(function () {
                smfunc.rpcGetinfoBothSides(function (privateInfo, publicInfo) {

                    logger.log('interval');

                    if (!privateInfo || !publicInfo) {
                        logger.log("RPC Error checking best heights");
                        //scheduleHeightCheck(interval);
                        return;
                    }

                    var heightPrivate = privateInfo.blocks;
                    var heightPublic = publicInfo.blocks;
                    logger.log("Private height = " + heightPrivate + " Public height = " + heightPublic);

                    addSeenHash(privateInfo.hash, heightPrivate, null, null, null, 'private');
                    addSeenHash(publicInfo.hash, heightPublic, null, null, null, 'public');

                    if (heightPrivate == heightPublic) {

                        clearInterval(timerId);

                        //stop the peers
                        shutdownPeer(peers.private);
                        shutdownPeer(peers.public);

                        //next stage, initialize the smManager's internal chains
                        doRPCInitializeChains(onCompleteCallback);



                    } else {
                        interval = Math.abs(heightPublic - heightPrivate) > 100 ? 3000 : 1000;

                        if (heightPrivate == lastPrivateHeight) {
                            timesSeenAtHeight++
                        } else {
                            timesSeenAtHeight = 0;
                        }

                        if (timesSeenAtHeight > 8) {
                            //peers might have gotten stuck, stop and restart them
                            //stop the peers

                            logger.log("Simple relays looks stuck, restarting");

                            lastPrivateHeight = 0;
                            timesSeenAtHeight = 0;

                            shutdownPeer(peers.private);
                            shutdownPeer(peers.public);

                            //start them after short delay
                            setTimeout(function() {
                                peers = startSimpleRelay(peerPrivateParams, peerPublicParams);
                            },1000);

                        } else {
                            logger.log("times seen at this height: " + timesSeenAtHeight);
                        }

                        lastPrivateHeight = heightPrivate;
                        //check again in 1 or 5 secs
                        //scheduleHeightCheck(interval);
                    }

                });
            }, interval);
        }

        scheduleHeightCheck(interval);

    }


    /**
     * once the chains have been sync'd by p2p we'll init smManager chain state by rpc querying the best heights
     * and get all the block hashes in between.
     * @param onCompleteCallback
     */
    function doRPCInitializeChains(onCompleteCallback) {

        var sidesComplete = 0;

        var heightPrivate = privateStartHeight;
        var heightPublic;

        var privateHeightsRequested = [];
        var publicHeightsRequested = [];


        smfunc.rpcGetinfoBothSides(onHeightsReturned);

        function onHeightsReturned(privateInfo, publicInfo) {
            if (!privateInfo || !publicInfo) {
                throw new Error('RPC error: ' + result[0].error.message);
            }

            heightPrivate = privateInfo.blocks;
            heightPublic = publicInfo.blocks;

            //make a list of heights for both sides
            var isPublicHeightest = heightPublic >= heightPrivate;
            var lowest = isPublicHeightest ? heightPrivate : heightPublic;
            var heighest = isPublicHeightest ? heightPublic : heightPrivate;
            lowest--; //go back one more block to be safe

            var privateCommands = [];
            var publicCommands = [];

            for (var i = lowest; i < heightPrivate; i++) {
                privateCommands.push(["getblockhash", [i]]);
                privateHeightsRequested.push(i);
            }

            for (var i = lowest; i < heightPublic; i++) {
                publicCommands.push(["getblockhash", [i]]);
                publicHeightsRequested.push(i);
            }

            smfunc.doubleRPCCall(privateCommands, publicCommands, processRpcBlockHashes);

        }

        function processRpcBlockHashes(privateError, privateResults, publicError, publicResults) {
            if (privateError) {
                throw new Error("RPC Error initialising: " + JSON.stringify(privateError));
            }
            if (publicError) {
                throw new Error("RPC Error initialising: " + JSON.stringify(publicError));
            }

            for (var i = 0; i < privateResults.length; i++) {
                var result = privateResults[i];
                if (result.error) {
                    throw new Error('RPC error initializing chain');
                }
                var hash = result.result;
                var height = privateHeightsRequested[i];
                addSeenHash(hash, height, null, null, null, 'private');
                smManager.addPrivateBlock(hash, height, true);
            }

            for (var i = 0; i < publicResults.length; i++) {
                var result = publicResults[i];
                if (result.error) {
                    throw new Error('RPC error initializing chain');
                }
                var hash = result.result;
                var height = publicHeightsRequested[i];
                addSeenHash(hash, height, null, null, null, 'public');
                smManager.addPublicBlock(hash, height, true);
            }

            //start the real peers
            var peers = startPrivatePublicRelay(peerPrivateParams, peerPublicParams);
            peerPrivate = peers.private;
            peerPublic = peers.public;
            allPeers[peers.private.id] = peers.private;
            allPeers[peers.public.id] = peers.public;

            //delay the other public peers because I feel like it
            setTimeout(function() {
                for (var i = 0; i < otherPeerParams.length; i = i + 2) {
                    var peerParams1 = otherPeerParams[i];
                    var peerParams2 = otherPeerParams[i + 1];
                    peers = startPublicPairRelay(peerParams1, peerParams2);
                    otherPeers[peers.peer1.id] = peers.peer1;
                    otherPeers[peers.peer2.id] = peers.peer2;
                    allPeers[peers.peer1.id] = peers.peer1;
                    allPeers[peers.peer2.id] = peers.peer2;
                 }
                 otherPeersReverseKeys = Object.keys(otherPeers).reverse()
                onCompleteCallback();
            }, 5000);
        }
    }

    // sorry about the lazy refactoring that follows - needed to reduce the noise in this file

    function getHashForMessage(message, index = 0) {
        return smfunc.getHashForMessage(message, index);
    }

    function getPrevHashForMessage(message, index = 0) {
        return smfunc.getPrevHashForMessage(message, index);
    }

    //a wrapper so smManager can access it
    this.getPrevHashForMessagePublic = function (message, index = 0) {
        return getPrevHashForMessage((messages, index))
    }

    /**
     * @param message
     * @return {*} number of hashes in the message
     */
    function getHashCountForMessage(message) {
        return smfunc.getHashCountForMessage(message);
    }

    /**
     * Determines if a message is a type that references one of more block hashes
     *
     * we currently include inv, getheaders, headers, getdata and block messages
     *
     * @param message
     * @return {boolean}
     */
    function isHashedMessage(message) {
        return smfunc.isHashedMessage(message);
    }


    function reverseBufferToString(buf) {
        return smfunc.reverseBufferToString(buf);
    }

    /**
     * Attempts to determine the block height of a message.  If the message contains multiple hashes it will determine the first.
     *
     * It is safer in this case to split the message into multiple single hash messages.
     *
     * @param message
     * @return {*}
     */
    function maybeGetBlockHeightForMessage(message) {
        return smfunc.maybeGetBlockHeightForMessage(message);
    }

    /**
     * Attempt to determine the block height of a given hash either from a previously cached version of the block hash
     * or from the previous hash.  Aside from early in the life of the pool when RPC calls are used to determine block
     * heights, they are usually determined by the previous hash already having a block height so it's best to always
     * pass the prevHash parameter if you have it available.
     *
     * For this reason inv messages will rarely be able to have height determined.  Once a headers message has been seen
     * it's fairly safe to assume block height will be able to be determined except in rare instances where they may arrive
     * out of order.
     *
     * @param hash
     * @param prevHash if known
     * @return {*}
     */
    function maybeGetBlockHeight(hash, prevHash = null) {
        return smfunc.maybeGetBlockHeight(hash, prevHash);
    }

    /**
     * Get an object with some meta data about a hash if it's already been seen.  If an object is returned it will have
     * a stucture like this:
     * {
     *      'hash': hash,
     *      'height': height,
     *      'prevHash': prevHash,
     *      'message': message,
     *      'seenPublic': false,
     *      'seenPrivate' : false
     * }
     *
     * @param hash
     * @return {*}
     */
    function getSeenHash(hash) {
        return smfunc.getSeenHash(hash);
    }

    /**
     * adds a hash to an internal map used for determining block height
     * @param hash
     * @param height
     * @param prevHash if known, otherwise pass null.
     * @param message
     * @param peer
     */
    function addSeenHash(hash, height, prevHash = null, message = null, peer = null, chain = null) {
        return smfunc.addSeenHash(hash, height, prevHash, message, peer, chain)
    }

    /**
     * Splits headers message into multipl messages all containing a single header entry
     * @param message
     */
    function splitHeadersMessage(message, peer) {
        return smfunc.splitHeadersMessage(message, peer);
    }


    function getMessageOutput(peer, message, targetPeer = null) {
        return smfunc.getMessageOutput(peer, message, targetPeer);
    }

    function getSuppressedMessageOutput(peer, message) {
        return smfunc.getSuppressedMessageOutput(peer, message);
    }

    /**
     * adds any headers to the seen cache and populates peer and height properties on the message.
     * @param message
     */
    function preprocessMessage(peer, message, isRelayPair = false) {
        //cache message heights
        var returnMessages = [message];
        if (message.command == 'headers') {
            var headerMessages = splitHeadersMessage(message, peer);
            for (var headerMessage of headerMessages) {
                var hash = getHashForMessage(headerMessage);
                var prevHash = getPrevHashForMessage(headerMessage);
                //addSeenHash will try to get height so pass null to avoid a double check
                addSeenHash(hash, null, prevHash, headerMessage, peer, peer.chain);
                smfunc.cacheHeadersMessage(hash, headerMessage);
            }
            returnMessages = headerMessages;
        }

        if (returnMessages.length == 1) {
            returnMessages[0].peer = peer;
            returnMessages[0].height = maybeGetBlockHeightForMessage(returnMessages[0]);
            returnMessages[0].receivedBy = peer.chain;
        }

        if (isRelayPair && (message.command == 'ping' || message.command == "pong")) {
            //suppress log messsage
        } else {
            logger.log(getMessageOutput(peer, message));
        }
        return returnMessages;
    }


    /**
     * After calling smManager to handle a new headers message it will return a list of public and private
     * blocks to broadcast based on the SM algorithm.  This function handles that broadcasting.
     * @param relayBlocks
     * @param originChain 'public' or 'private'
     * @return {boolean} true if the original message should be relayed.
     */
    function processRelayBlocks(relayBlocks, originChain) {

        for (var chain of ['private', 'public']) {

            for (var meta of relayBlocks[chain]) {

                //a couple of sanity checks
                if (!meta.message)
                    throw new Error('relayBlock missing message');
                if (!meta.message.command == 'headers')
                    throw new Error('relayBlock message is not headers type, found: ' + meta.message.command);

                if (chain == 'private') {

                    if (maybeSendMessage(peerPrivate, meta.message)) {
                        //add it the smManagers private side
                        smManager.addPrivateBlock(meta.hash, meta.height, false, meta.message);
                    }

                } else {
                    //send to public chain
                    if (maybeSendMessage(peerPublic, meta.message)) {
                        //add it the smManagers public side
                        smManager.addPublicBlock(meta.hash, meta.height, false, meta.message);
                        markPrivateMessageSentTo(meta.hash, peerPublic, meta.message.command);

                        //get the block message if we have it (it should have been prefetched)
                        var blockMessage = getPrivateMessage(meta.hash, meta.height, 'block', peerPublic);
                        if (blockMessage) {
                            if (maybeSendMessage(peerPublic, blockMessage)) {
                                logger.log(getMessageOutput(peerPublic, blockMessage));
                                markPrivateMessageSentTo(meta.hash, peerPublic.id, 'block');
                            }
                        }
                    }

                    //iterate otherPeers and send to any that haven't seen it yet, most likely all of them since
                    //this is a headers message that has previously been withheld.
                    var alreadySentPeers = []; //for sanity check
                    var sentHeadersNowPeers = [];
                    var sentBlockNowPeers = [];
                    for (var id of otherPeersReverseKeys) {
                        var otherPeer = otherPeers[id];
                        if (!isPrivateMessageSentTo(meta.hash, id, meta.message.command)) {
                            if (maybeSendMessage(otherPeer, meta.message)) {
                                markPrivateMessageSentTo(meta.hash, id, meta.message.command);
                                sentHeadersNowPeers.push(otherPeer.name);
                            }
                        } else {
                            alreadySentPeers.push(otherPeer);
                        }
                        // if (!isPrivateMessageSentTo(meta.hash, id, 'block')) {
                        //     //get the block message if we have it (it should have been prefetched)
                        //     var blockMessage = getPrivateMessage(meta.hash, meta.height, 'block', otherPeer);
                        //     if (blockMessage) {
                        //         if (maybeSendMessage(otherPeer, blockMessage)) {
                        //             markPrivateMessageSentTo(meta.hash, id, 'block');
                        //             sentBlockNowPeers.push(otherPeer.name);
                        //         }
                        //     }
                        // }
                    }
                    logger.log("pushed headers message [" + meta.height +"] to peers: " + sentHeadersNowPeers);
                    logger.log("pushed block message [" + meta.height +"] to peers: " + sentBlockNowPeers);
                    //sanity check
                    if (alreadySentPeers.length > 0) {
                        //throw new Error('One or more otherPeers already registered as sent headers message.  How the hell did that happen?');
                    }

                }

            }

        }

        return true;
    }

    /**
     * tell the smManager to handle multiple messages and consolidate the results.
     *
     * @param message
     * @param handleFunction
     */
    function handleSplitHeaders(messages, peer, handleFunction) {

        var relayMessages = {'private': [], 'public': []};
        var addedHashes = {'private': {}, 'public': {}};

        for (var message of messages) {

            if (message.command != 'headers')
                throw new Error('Message type is not headers');

            if (!message.height) {
                logger.log('WARNING - Skipping block due to unresolved height: ' + message.hash);
                continue;
            }

            var thisRelayBlocks = handleFunction(message.hash, message.height, message);

            for (var chain of ['private', 'public']) {

                for (var blockMeta of thisRelayBlocks[chain]) {
                    //check we haven't already added it
                    if (!(blockMeta.hash in addedHashes[chain])) {
                        relayMessages[chain].push(blockMeta);
                        addedHashes[chain][blockMeta.hash] = null; //using it as a set
                    }
                }
                relayMessages[chain].sort(function (a, b) {
                    return a.height - b.height;
                });
                //FIXME just checking we got the ordering right - get rid of this once sure
                if (relayMessages[chain].length > 1) {
                    if (relayMessages[chain][0].height > relayMessages[chain][1].height)
                        throw new Error('you screwed up the sort comparator');
                }
            }
        }
        return relayMessages;

    }

    /**
     * relay between private SM node and a public node... Mainly exists to keep the private peer happy so we can remain connected
     * and see it's privately mined blocks.
     *
     * @param peerPrivateParams
     * @param peerPublicParams
     */
    function startPrivatePublicRelay(peerPrivateParams, peerPublicParams) {

        var relayFunction = function (peer, message) {
            //this turns it in an array
            var messages = preprocessMessage(peer, message);
            var chain = peer.chain;

            var doRelay = true;

            //register messages in private message store
            if (peer.isPrivate) {
                //register private messages
                if (message.command == 'headers' || message.command == 'block') {
                    for (var splitMessage of messages) {
                        if (splitMessage.hash && splitMessage.height) {
                            registerPrivateMessage(splitMessage.hash, splitMessage.height, splitMessage, false);
                        }
                    }
                    //headers relay is done as part of processRelayBlocks
                    //block relay is done in response to getdata
                    doRelay = false;
                }
                //if this is a headers message from private peer send a getdata to prefetch the block
                if (message.command == 'headers') {
                    for (var splitMessage of messages) {
                        if (splitMessage.hash && splitMessage.height) {
                            //check if we already have it
                            if (!isPrivateBlockRequestedOrPresent(splitMessage.hash, splitMessage.height)) {
                                var getdata = _messages.GetData.forBlock(splitMessage.hash);
                                if (maybeSendMessage(peer, getdata)) {
                                    //mark it requested so we don't double up
                                    markPrivateBlockRequested(splitMessage.hash, splitMessage.height);
                                }
                            }
                        }
                    }
                }
                if (message.command == 'getheaders') {
                    var firstHash = getHashForMessage(message);
                    if (firstHash) {
                        var requestedInvEntry = getRequestedInvEntry(firstHash)
                        if (requestedInvEntry && !requestedInvEntry.sentToAll) {
                            requestedInvEntry.getheadersMessage = message;
                            //send to all peers
                            doRelay = true; //also send to public peer
                            for (var id of otherPeersReverseKeys) {
                                var otherPeer = otherPeers[id];
                                maybeSendMessage(otherPeer, message, false);
                            }
                            requestedInvEntry.sentToAll = true;
                            logger.log("recieved getheaders from peerPrivate, relayed to all otherPeers [" + firstHash + "]");
                        }
                    }
                }


            } else {
                //public peer
                if (message.command == 'getdata') {
                    //send a cached block or register to have it sent as soon as we see it.

                    for (var splitMessage of messages) {
                        if (splitMessage.hash && splitMessage.height) {
                            //this function will register the peer to receive the message as soon as it arrives
                            //if it hasn't already.
                            var blockMessage = getPrivateMessage(splitMessage.hash, splitMessage.height, 'block', peer, true);
                            if (blockMessage) {
                                if (maybeSendMessage(peer, blockMessage)) {
                                    markPrivateMessageSentTo(splitMessage.hash, peer.id, 'block');
                                    doRelay = false;
                                }
                                logger.log(peer.name + ' sent getdata, returning cached block message [' + splitMessage.height + ']');
                            } else {
                                logger.log(peer.name + ' sent getdata, cache miss for block message [' + splitMessage.height + ']');
                            }
                        }
                    }
                }
            }

            //save the getheaders message in case we need to retry sending it
            //Peers are really finnicky about accepting oddly formed getheaders messages so
            //best to use one the node generated.
            if (message.command == 'getheaders') {
                if (peer.chain == 'private')
                    lastGetHeadersPrivate = message;
                else
                    lastGetHeadersPublic = message;
            }

            if (message.command == 'headers') {

                var relayFunction = chain == 'public' ? smManager.handlePublicMinedBlock : smManager.handlePrivateMinedBlock;
                if (messages.length == 2)
                    var x = 0; //breakpoint
                var relayBlocks = handleSplitHeaders(messages, peer, relayFunction);
                processRelayBlocks(relayBlocks, peer.chain);

            } else if (doRelay) {
                maybeSendMessage(peer.pairedPeer, message, false);
            } else {
                logger.log("aborting message relay: " + message.command + "[" + message.height + ']');
            }
        };
        return connectAndJoinPeers(peerPrivateParams, peerPublicParams, relayFunction, relayFunction, 'private', 'public');
    }

    /**
     * A relay between two public peers that behaves like a plain relay.  This enables us to see publicly mined blocks
     * and gives us a way to widely broadcast our private blocks when needed.
     *
     * @param peerParams1
     * @param peerParams2
     * @return {{peer1: *, peer2: *}}
     */
    function startPublicPairRelay(peer1Params, peer2Params) {

        var relayFunction = function (peer, message) {


            //this turns it into an array
            var messages = preprocessMessage(peer, message, true);

            //for first 30 seconds let the peers talk to each normally
            var time = (new Date()).getTime();
            if (time < peer.startTime + 30000) {
                maybeSendMessage(peer.pairedPeer, message, false);
                return;
            }

            var doRelay = true;

            if (message.command == 'headers') {

                var relayBlocks = handleSplitHeaders(messages, peer, smManager.handlePublicMinedBlock);
                //if any public blocks need relaying to the private peer we'll send those.
                processRelayBlocks(relayBlocks, 'public');
                //don't bother relaying these headers to the public peer pairs.  They can sort their own shit out
                //using other connections.


            } else if (isHashedMessage(message)) {

                //non-headers messages - inv, getheaders, getdata, block
                //since we are suppressing headers messages between public peer pairs we should only
                //be seing getdata and getheaders requests from the public peers in response to private
                //headers messages we have sent out.

                //messages to send back to the peer
                var returnMessages = [];
                //message to relay to the ??? peer
                var relayMessages = [];
                var seenHashes = {};

                var len = getHashCountForMessage(message);
                for (var i = 0; i < len; i++) {

                    var hash = getHashForMessage(message, i);
                    var prevHash = getPrevHashForMessage(message, i);
                    var height = maybeGetBlockHeight(hash, prevHash);

                    if (hash in seenHashes) {
                        //duplicate hash - likely this is a getdata or getheaders message
                        continue;
                    }
                    seenHashes[hash] = null;

                    if (height) {

                         if (message.command == 'getdata') {

                            //check if we've got a cached private block message
                            //if we don't this automatically registers the peer as waiting for the block
                            //message so as soon as the private give it to us it will be sent.
                            var returnMessage = getPrivateMessage(hash, height, 'block', peer, true);

                            //if we didn't get a return message this may be a public block so we can just ignore it.
                            if (returnMessage) {
                                returnMessages.push(returnMessage);
                                logger.log(peer.name + ' sent getdata, returning cached block message [' + height + ']');
                            } else {
                                logger.log(peer.name + ' sent getdata, cache miss for block message [' + height + ']');
                            }
                            dumpPrivateMessageCacheState('startPublicPairRelay get: ', hash);

                        } else if (message.command == 'getheaders') {

                            //just behave normally for the first few headers messages so the other node
                            //doesn't think we are misbehaving.  After this we won't relay inv or headers
                            //messages so the only getheaders messages we should see are after we send a private
                            //headers message which means we should have it in the privateMessage cache.
                            var returnMessage = getPrivateMessage(hash, height, 'headers', peer, true);
                            if (returnMessage) {
                                returnMessages.push(returnMessage);
                                logger.log(peer.name + ' sent getheaders, returning cached headers message [' + hash + ']');
                            }
                            if (peer.getheadersMessagesRelayed <= 2) {
                                if (relayMessages.length == 0)
                                    relayMessages.push(message);
                            }

                        }
                    } else {

                        //couldn't get height - if it's an inv message this should be normal unless
                        //we've already seen a related headers or block message in which case the
                        //next part doesn't need to be done.
                        if (message.command = 'inv') {

                            //if a hm node finds a block we'll likely see an inv message from them before
                            //we see it from our public peer.  To make sure we don't give the honest miners
                            //a head start we respond with a getheaders message immediately.

                            if (!isRequestedInv(hash)) {

                                // //use an existing getheaders message and replace the first start
                                // //since these are tricky to build.
                                // var getHeadersMessage = lastGetHeadersPublic;
                                // getHeadersMessage.starts[0] = message.inventory[0].hash
                                // if (maybeSendMessage(peer, getHeadersMessage, false)) {
                                //     logger.log(peer.name + ' sent inv, returning getheaders message [' + hash + ']');
                                //     registerRequestedInvEntry(hash);
                                // }

                                //relay to the private node to generate a getheaders message which we'll
                                //send to all nodes
                                if (maybeSendMessage(peerPrivate, message, false)) {
                                    logger.log(peer.name + ' sent inv, relaying to peerPrivate [' + hash + ']');
                                    registerRequestedInvEntry(hash);
                                }

                            }

                        }

                        //couldn't get height so ignore the message in case it's for a competing block we haven't seen.

                    }
                }

                //send any responses back
                for (var returnMessage of returnMessages) {
                    maybeSendMessage(peer, returnMessage);
                }

                // relay messages to other public peer
                for (var relayMessage of relayMessages) {
                    maybeSendMessage(peer.pairedPeer, relayMessage, false);
                }

            }  else {
                //non-hashed message - ver, verack, addr etc - just relay it
                maybeSendMessage(peer.pairedPeer, message);

            }

    }
    return connectAndJoinPeers(peer1Params, peer2Params, relayFunction, relayFunction, 'peer1', 'peer2');
}

/**
 * Send a message if peer is connected and ready
 * @param peer
 * @param message
 * @return {boolean} true if message was sent
 */
function maybeSendMessage(peer, message, errorOnUnsplit = true, forceResend = true) {

    //sanity check
    if (errorOnUnsplit && getHashCountForMessage(message) > 1)
        throw new Error('messages should be split before relaying');
    if (peer.ready) {
        if (peer.isPrivate && isHashedMessage(message)) {

            //only send message if private hasn't seen it yet.
            if (!forceResend && isRelayedToPrivate(message)) {
                //private has already seen this message
                logger.log('aborting ' + message.command + ' message to ' + peer.name + ' - private message already seen');
                return false;
            } else {
                registerRelayedToPrivateEntry(message);
            }
        }
        peer.sendMessage(message);
        return true;
    } else {
        logger.log('aborting ' + message.command + ' to peer ' + peer.name + ' - not ready');
    }
    return false;
}

/**
 * A simple relay pair used to let both sides equalize block heights
 * @param peer1Params
 * @param peer2Params
 * @return {*}
 */
function startSimpleRelay(peer1Params, peer2Params) {

    var relayFunction = function (peer, message) {
        preprocessMessage(peer, message);
        maybeSendMessage(peer.pairedPeer, message, false);
    };
    return connectAndJoinPeers(peer1Params, peer2Params, relayFunction, relayFunction, 'private', 'public');
}

function shutdownPeer(peer) {
    peer.reconnectOnDisconnect = false;
    peer.disconnect();
}

function connectAndJoinPeers(peer1Params, peer2Params, peer1RelayFunction, peer2relayFunction, peer1Key, peer2Key) {
    var peer1 = makePeer(peer1Params, peer1RelayFunction);
    var peer2 = makePeer(peer2Params, peer2relayFunction);
    peer1.pairedPeer = peer2;
    peer2.pairedPeer = peer1;
    peer1.connect();
    peer2.connect();
    var result = {};
    result[peer1Key] = peer1;
    result[peer2Key] = peer2;
    return result;
}

function makePeer(peerParams, relayFunction) {

    if (typeof peerParams.network == 'string')
        peerParams.network = Networks[peerParams.network];

    var name = peerParams.name;
    var peer = new Peer(peerParams);
    peer.id = 'id' in peerParams ? peerParams.id : null;
    peer.name = peer.id == null ? name : name + '[' + peer.id + ']';
    peer.pairedPeer = null;
    peer.chain = peerParams.chain;
    peer.isPrivate = peer.chain == 'private';
    peer.reconnectOnDisconnect = true;
    peer.startTime = (new Date()).getTime();

    peer.on('ready', function () {

        logger.log(name + ' ' + peer.version + ', ' + peer.subversion + ', ' + peer.bestHeight);
        peer.connected = true;
        peer.ready = true;

    });

    peer.on('connect', function () {
        peer.connected = true;
        logger.log(name + ' connect');

    });

    peer.on('disconnect', function () {
        logger.log(name + ' disconnect');
        peer.ready = false;
        peer.connected = false;
        if (peer.reconnectOnDisconnect) {
            setTimeout(function() {
                peer.connect();
                peer.connected = true;
            }, 1000);
        }
    });

    peer.on('error', function (error) {
        logger.log(peer.name + ' - ' + error);
        // peer.ready = false;
        // peer.connected = false;
        // if (peer.reconnectOnDisconnect) {
        //     setTimeout(function() {
        //         peer.connect();
        //         peer.connected = true;
        //     }, 3000);
        // }

    });

    peer.on('ver', function (message) {
        relayFunction(this, message);
    });

    peer.on('verack', function (message) {
        //relayFunction(this, message);
    });

    peer.on('reject', function (message) {
        relayFunction(this, message);
    });

    peer.on('ping', function (message) {
        if (!relayFunction(this, message)) {
            var message = messages.Pong(message.nonce);
            if (this.pairedPeer)
                maybeSendMessage(this.pairedPeer, message);
        }
    });

    peer.on('pong', function (message) {
        relayFunction(this, message);
    });


    peer.on('addr', function (message) {
        relayFunction(this, message);
    });

    peer.on('getaddr', function (message) {
        relayFunction(this, message);
    });

    peer.on('inv', function (message) {
        relayFunction(this, message);
    });

    peer.on('getheaders', function (message) {
        relayFunction(this, message);
    });

    peer.on('headers', function (message) {
        relayFunction(this, message);
    });

    peer.on('getblocks', function (message) {
        relayFunction(this, message);
    });

    peer.on('getdata', function (message) {
        relayFunction(this, message);
    });

    peer.on('block', function (message) {
        relayFunction(this, message);
    });

    peer.on('mempool', function (message) {
        relayFunction(this, message);
    });

    peer.on('merkleblock', function (message) {
        relayFunction(this, message);
    });

    peer.on('notfound', function (message) {
        relayFunction(this, message);
    });

    peer.on('tx', function (message) {
        relayFunction(this, message);
    });

    peer.on('feefilter', function (message) {
        relayFunction(this, message);
    });

    peer.on('filteradd', function (message) {
        relayFunction(this, message);
    });

    peer.on('filterclear', function (message) {
        relayFunction(this, message);
    });

    peer.on('filterload', function (message) {
        relayFunction(this, message);
    });

    return peer;
}


}
