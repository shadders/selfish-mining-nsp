#!/usr/bin/env bash

TIMEOUT=$1

if [ -z "$TIMEOUT" ]; then
  TIMEOUT=3s
fi

#ensure bitcoind isn't running
#attempt graceful shutdown
/root/coinds/bitcoin-sm/src/bitcoin-cli stop
sleep $TIMEOUT

#force it if necessary
pkill -9 bitcoind
rm /root/bitcoin/.lock

#make sure all minerd instances are stopped as well
killall minerd
pkill -9 minerd
