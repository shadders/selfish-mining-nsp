#!/usr/bin/env bash

echo "init-pool-pull" > /root/state/mine_mode.txt
echo "`date -u` init-pool-pull" >> /root/state/mine_mode_log.txt

cd /root/pool/node-stratum-pool
git reset --hard HEAD
git pull
find ./ -name "*.sh" -exec chmod +x {} \;
chmod +x cpuminer-bin/minerd

#npm update

echo "pool-pull-complete" > /root/state/mine_mode.txt
echo "`date -u` pool-pull-complete" >> /root/state/mine_mode_log.txt
