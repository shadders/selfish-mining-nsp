/**
 * Created by Steve on 20/06/2017.
 */

var os = require('os');
var path = require('path');
var fs = require('fs');
var argv = require('minimist')(process.argv.slice(2));
var mkdirp = require('mkdirp');

var hostbase = 'test';
var basedir = process.platform == 'linux' ? '' : './os';

validateArgs();

if (argv['phase'] == '1')
    main1();
else if (argv['phase'] == '2')
    main2();
else
    console.log('No valid install phase specified')
process.exit();


function validateArgs() {
    var all_args = ['phase', 'pip', 'host', 'mip1', 'mip2', 'smpip'];
    for (var arg of all_args) {
        if (!(arg in argv)) {
            usage();
            process.exit();
        }
    }
}

function main1() {
    //console.dir(argv);

    var shellCommands = [];

    var conf = {
    };
    conf.private_ip = argv['pip'];
    conf.hostname = argv['host'];
    conf.node_num = conf.hostname.replace(hostbase, '');
    conf.master_ip1 = argv['mip1'];
    conf.master_ip2 = argv['mip2'];
    conf.sm_pool_ip = argv['smpip'];
    conf.rpc_user = argv['rpcu'];
    conf.rpc_password = argv['rpcpw'];

    writeStringToFile(basedir + '/root/server_config.json', JSON.stringify(conf, null, 4));

    console.log(`Initializing phase one install with private ip: ${conf.private_ip}`);

    //write out environment variables
    var envString = `
export PRIVATE_IP=${conf.private_ip}
export HOSTNAME=${conf.hostname}
export NODE_NUM=${conf.node_num}
export MASTER_IP1=${conf.master_ip1}
export MASTER_IP2=${conf.master_ip2}
export SM_POOL_IP=${conf.sm_pool_ip}
`;

    var content = readFileAsString(basedir + '/etc/environment');
    var output = replaceBetween('#BEGIN_REPLACE', '#END_REPLACE', content, envString);
    writeStringToFile(basedir + '/etc/environment', output);
    //console.log(output);

    //write out private network config
    var netString = `
auto ens7
iface ens7 inet static
    address ${conf.private_ip}
    netmask 255.255.0.0
    mtu 1450
`

    content = readFileAsString(basedir + '/etc/network/interfaces');
    output = replaceBetween('#BEGIN_INSERT_PRIVATE_NETWORK', '#END_INSERT_PRIVATE_NETWORK', content, netString);
    writeStringToFile(basedir + '/etc/network/interfaces', output);
    //console.log(output);

    shellCommands.push('ifup ens7'); //enable private network interface

    //write out bitcoin conf
    content = readFileAsString('bitcoin.conf');
    content = content.replace(new RegExp('PRIVATE_IP', 'g'), conf.private_ip);
    content = content.replace(new RegExp('MASTER_IP1', 'g'), conf.master_ip1);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/.bitcoin/bitcoin.conf', content);

    //update the sm node confs
    var replaceFile = ''

    replaceFile = basedir + '/root/.bitcoin1/bitcoin.conf';
    content = readFileAsString(replaceFile);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(replaceFile, content);

    replaceFile = basedir + '/root/.bitcoin1/bitcoin-isolated.conf';
    content = readFileAsString(replaceFile);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(replaceFile, content);

    replaceFile = basedir + '/root/.bitcoin2/bitcoin.conf';
    content = readFileAsString(replaceFile);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(replaceFile, content);

    //write out sm pool config
    content = readFileAsString('../sm/pool-template.json');
    content = content.replace(new RegExp('MASTER_IP1', 'g'), conf.master_ip1);
    content = content.replace(new RegExp('PRIVATE_IP', 'g'), conf.private_ip);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/conf/sm/pool.json', content);

    //write out sm pool config - peers file
    content = readFileAsString('../sm/peers-master-template.json');
    content = content.replace(new RegExp('MASTER_IP1', 'g'), conf.master_ip1);
    content = content.replace(new RegExp('PRIVATE_IP', 'g'), conf.private_ip);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/conf/sm/peers-master.json', content);

    console.log(`Phase one install complete...`);

    process.exit();
}

function main2() {
    var shellCommands = [];

    var conf = {
    };
    conf.private_ip = argv['pip'];
    conf.hostname = argv['host'];
    conf.node_num = conf.hostname.replace(hostbase, '');
    conf.master_ip1 = argv['mip1'];
    conf.master_ip2 = argv['mip2'];
    conf.sm_pool_ip = argv['smpip'];
    conf.bitcoin_address = argv['addr'];
    conf.rpc_user = argv['rpcu'];
    conf.rpc_password = argv['rpcpw'];

    writeStringToFile(basedir + '/root/server_config.json', JSON.stringify(conf, null, 4));

    if (!conf.bitcoin_address) {
        console.log('No bitcoin address supplied, phase 2 install aborted');
        system.exit();
    }


    console.log(`Initializing phase two install with private ip: ${conf.private_ip}`);

    //write out pool config
    content = readFileAsString('pool/pool.json');
    content = content.replace(new RegExp('COINBASE_ADDRESS', 'g'), conf.bitcoin_address);
    content = content.replace(new RegExp('MASTER_IP1', 'g'), conf.master_ip1);
    content = content.replace(new RegExp('PRIVATE_IP', 'g'), conf.private_ip);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/conf/pool.json', content);


    //write out cpuminer start scripts

    //mine local daemon
    content = readFileAsString('cpuminer/mine-local-daemon.sh');
    content = content.replace(new RegExp('COINBASE_ADDRESS', 'g'), conf.bitcoin_address);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/cpuminer-bin/mine-local-daemon.sh', content);

    //mine local daemon
    content = readFileAsString('cpuminer/mine-local-pool.sh');
    content = content.replace(new RegExp('COINBASE_ADDRESS', 'g'), conf.bitcoin_address);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/cpuminer-bin/mine-local-pool.sh', content);

    content = readFileAsString('cpuminer/mine-sm-pool.sh');
    content = content.replace(new RegExp('COINBASE_ADDRESS', 'g'), conf.bitcoin_address);
    content = content.replace(new RegExp('SM_POOL_IP', 'g'), conf.sm_pool_ip);
    content = content.replace(new RegExp('RPC_USER', 'g'), conf.rpc_user);
    content = content.replace(new RegExp('RPC_PASSWORD', 'g'), conf.rpc_password);
    writeStringToFile(basedir + '/root/pool/node-stratum-pool/cpuminer-bin/mine-sm-pool.sh', content);

    console.log(`Phase two install complete...`);
}

function readFileAsString(file) {
    return fs.readFileSync(file, 'utf-8');
}

function writeStringToFile(file, data) {
    makeParent(file);
    fs.writeFileSync(file, data, 'utf-8');
}

function makeParent(file) {
    makeDirs(path.dirname(file));
}

function makeDirs(dir) {
    if (fs.existsSync(dir)) {
        return true;
    }
    makeDirs(path.dirname(dir));
    fs.mkdirSync(dir);
}

function replaceBetween(before, after, string, insertable) {
    var start = string.indexOf(before);
    if (start < 0)
        throw new Error('begin marker string not found');
    start += before.length;

    var end = string.indexOf(after);
    if (end < 0)
        throw new Error('end marker string not found');

    var out = string.slice(0, start) + insertable + string.slice(end);
    return out;
}

function usage() {
    console.log(`Usage:
node tempaltes/install.js --phase <1 or 2> --pip <PRIVATE_IP> --host <HOSTNAME> --mip1 <MASTER_IP1> --mip2 <MASTER_IP2> --smpip <SM_POOL_IP>`);
}