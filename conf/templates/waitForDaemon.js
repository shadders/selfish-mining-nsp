/**
 * Created by Steve on 20/06/2017.
 */

var path = require('path');

var daemonPath = '../../lib/daemon.js';
daemonPath = path.resolve(daemonPath);

var daemon = require(daemonPath);
var _this = this;
var d = null;
var daemonConfig = null;

if (process.platform == 'linux') {
    var args = process.argv.slice(2);
    if (args.length < 2) {
        console.log('Must supply arguments RPC_USER and RPC_PASSWORD')
        process.exit();
    }
    daemonConfig = {
    "host": "localhost",
    "port": 19001,
    "user": args[0],
    "password": args[1]
};
} else {
    daemonConfig = {
        "host": "localhost",
        "port": 8233,
        "user": "barry_the_pool",
        "password": "DKH32fho23h098hf2ecnuoi"
    };
}

var options = {
    'daemons': [daemonConfig]
}

var emitErrorLog   = function(text) {
    _this.emit('log', 'error'  , text);
    console.log('ERROR: ' + text);
};

SetupDaemonInterface(function() {

    setInterval(function() {
        d.cmd('getblocktemplate', [], function(result) {
            if (result[0].error) {
                console.log('ERROR: ' + result[0].error.message);
            } else {
                console.log('DAEMON ONLINE...');
                process.exit();
            }
        }) ;
    }, 2000);

});

function SetupDaemonInterface(finishedCallback){

    if (!Array.isArray(options.daemons) || options.daemons.length < 1){
        emitErrorLog('No daemons have been configured - pool cannot start');
        return;
    }

    d = new daemon.interface(options.daemons, function(severity, message){
        console.log(message);
    });



    d.once('online', function(){
        finishedCallback();

    }).on('connectionFailed', function(error){
        emitErrorLog('Failed to connect daemon(s): ' + JSON.stringify(error));

    }).on('error', function(message){
        emitErrorLog(message);

    });

    d.init();
}
