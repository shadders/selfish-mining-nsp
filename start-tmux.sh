#!/bin/bash -x

export MINE_TYPE="$1"
export SM_POOL_IP="10.99.0.16"

if [[ $MINE_TYPE =~ ^.+$ ]]; then
  echo "Initializing with mine type: $MINE_TYPE"
else
  echo "mine type not specified as first argument"
  echo "valid options: local-daemon, local-pool, sm-pool"
  exit 1
fi

ROLE=`cat /root/state/role.txt`
MAX_CONNS=""

if [ $ROLE = "master" ]; then
    MAX_CONNS=" -maxconnections=300"
fi

#set global env variables
. /root/pool/node-stratum-pool/conf/setenv.sh
#load rpc auth
. /root/set_rpc_auth.sh

cd /root/pool/node-stratum-pool

echo "$MINE_TYPE-starting" > /root/state/mine_mode.txt
echo "`date -u` $MINE_TYPE-starting" >> /root/state/mine_mode_log.txt

#ensure bitcoind isn't running
. /root/pool/node-stratum-pool/stop-daemon.sh 3s

#kill existing tmux session in case it exists
killall tmux

tmux start-server

tmux -2 new-session -d -s root

# Setup a bitcoind window
tmux new-window -t root:1 -n 'Bitcoind'
tmux send-keys "cd /root/coinds/bitcoin-sm/src/; ./bitcoind -daemon ${MAX_CONNS} -whitelist=${SM_POOL_IP}" C-m

#start the sm nodes if this is master
#if [ "$ROLE" = 'master' ]; then
#    # Setup a pool
#    tmux new-window -t root:6 -n 'Make'
#    tmux send-keys "cd /root/pool/node-stratum-pool/conf/templates/sm-coinds; make stop; make start-isolated" C-m
#fi

echo waiting for bitcoind to start
cd /root/pool/node-stratum-pool/conf/templates
node waitForDaemon.js $RPC_USER $RPC_PASSWORD
#sleep $BITCOIND_STARTUP_DELAY
tmux send-keys "./bitcoin-cli getinfo" C-m

# Limit bitcoind CPU usage
tmux new-window -t root:2 -n 'cpulimit-btc'
tmux send-keys "cpulimit -e bitcoind -l 40" C-m

if [ $MINE_TYPE != "daemon-only" ]; then

    # Setup a pool
    tmux new-window -t root:3 -n 'Stratum pool'
    tmux send-keys "cd /root/pool/node-stratum-pool; node init.js  |& tee -a /root/state/pool.log" C-m

    # Start miner
    #wait 5s for pool to start
    sleep 5s
    tmux new-window -t root:4 -n 'Miner'
    tmux send-keys "cd /root/pool/node-stratum-pool/cpuminer-bin; ./mine-${MINE_TYPE}.sh" C-m

    # Limit miner CPU usage
    tmux new-window -t root:5 -n 'cpulimit-miner'
    tmux send-keys "cpulimit -e minerd -l 40" C-m

fi

# Set default window
tmux select-window -t root:1

echo "$MINE_TYPE-started" > /root/state/mine_mode.txt
echo "`date -u` $MINE_TYPE-started" >> /root/state/mine_mode_log.txt

# Attach to session
tmux -2 attach-session -t root