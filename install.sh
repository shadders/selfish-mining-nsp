#!/bin/bash

#set -x

echo "install-phase1" > /root/state/mine_mode.txt
echo "`date -u` install-phase1" >> /root/state/mine_mode_log.txt

#set global env variables
. /root/pool/node-stratum-pool/conf/setenv.sh
#load rpc auth
. /root/set_rpc_auth.sh

cd /root/pool/node-stratum-pool/

### INSTALL SCRIPT

#ensure bitcoind isn't running
pkill -9 bitcoind
rm /root/bitcoin/.lock

HOST_PREFIX="10.99.0."

export MASTER_IP1="10.99.0.17"
export MASTER_IP2="10.99.0.112"
export SM_POOL_IP="10.99.0.16"

#copy selfish miner configs - only used on the sm-pool and master node
cp -r conf/templates/sm-coinds/.bitcoin1 /root/
cp -r conf/templates/sm-coinds/.bitcoin2 /root/
#make sure they're clean
cd conf/templates/sm-coinds
make clean
cd /root/pool/node-stratum-pool/

export MODE="$1"
export PRIVATE_IP="$2"
export ROLE="$3"

if [[ $PRIVATE_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  echo "Initializing with IP $PRIVATE_IP"
  echo $PRIVATE_IP > /root/state/private_ip.txt
  if [ "$ROLE" = "" ]; then
    echo "No role specified as 3rd argument"
    exit 1
  fi
  echo $ROLE > /root/state/role.txt
else
  if [ "$MODE" = "update" ]; then
    MODE="update"
    PRIVATE_IP=`cat /root/state/private_ip.txt`
    ROLE=`cat /root/state/role.txt`
    echo "Initializing with IP $PRIVATE_IP as role $ROLE"

  else
    echo "IP or 'update' not specified as 2nd argument"
     echo "usage: ./install.sh new <PRIVATE_IP> <ROLE> or ./install.sh update"
    exit 1
  fi
fi

if [ "$MODE" != "new" ]; then
  MODE=update
fi

echo "installing as $ROLE in $MODE mode"

export HOSTNAME="$(hostname)"
export NODE_NUM=`echo $PRIVATE_IP | sed "s/${HOST_PREFIX}//g"`

#make part of permanent environment
LINES=`cat <<EOT
export PRIVATE_IP=$PRIVATE_IP
export ROLE=$ROLE
export HOSTNAME=$HOSTNAME
export NODE_NUM=$NODE_NUM
export MASTER_IP1=$MASTER_IP1
export MASTER_IP2=$MASTER_IP2
export SM_POOL_IP=$SM_POOL_IP
EOT
`
echo commencing phase one install

cd /root/pool/node-stratum-pool/conf/templates
node install.js --phase 1 --pip $PRIVATE_IP --host $HOSTNAME --mip1 $MASTER_IP1 --mip2 $MASTER_IP2 --smpip $SM_POOL_IP --rpcu $RPC_USER --rpcpw $RPC_PASSWORD

#enable private interface
ifdown ens7
ip addr flush ens7
ifup ens7

if [ "$MODE" = "new" ]; then

    #bitcoin conf should exist now so we can start coind and get an address

#    if [ "$ROLE" = 'master' ]; then
#        #need another node started so use make nodes
#        #reset bitcoin1 and bitcoin2
#        cd /root/pool/node-stratum-pool/conf/templates/sm-coinds
#        make stop
#        make start-isolated
#        cd /root/pool/node-stratum-pool
#    fi

    #ensure we don't have a wallet from install image
    rm /root/.bitcoin/wallet.dat
    rm /root/.bitcoin/regtest/wallet.dat

    #clear state and log data
    rm /root/state/minerd.log
    rm /root/state/pool.log
    #rm /root/state/private_ip.txt
    rm /root/state/wallet_address.txt

    /root/coinds/bitcoin-sm/src/bitcoind -daemon

    #wait for it to start
    echo waiting for bitcoind to start
    cd /root/pool/node-stratum-pool/conf/templates
    node waitForDaemon.js $RPC_USER $RPC_PASSWORD
    #sleep $BITCOIND_STARTUP_DELAY

    export ADDRESS=`/root/coinds/bitcoin-sm/src/bitcoin-cli getnewaddress`
    echo "$ADDRESS" > /root/state/wallet_address.txt

else
    ADDRESS=`cat /root/state/wallet_address.txt`
fi

echo "Got local bitcoin address: $ADDRESS"

#start phase 2 install with bitcoin address

echo "install-phase2" > /root/state/mine_mode.txt
echo "`date -u` install-phase2" >> /root/state/mine_mode_log.txt

cd /root/pool/node-stratum-pool/conf/templates
node install.js --phase 2 --pip $PRIVATE_IP --host $HOSTNAME --mip1 $MASTER_IP1 --mip2 $MASTER_IP2 --smpip $SM_POOL_IP --addr $ADDRESS --rpcu $RPC_USER --rpcpw $RPC_PASSWORD

# make sure all scripts are executable
find /root/pool/node-stratum-pool/ -name "*.sh" -exec chmod +x {} \;

echo "install-complete" > /root/state/mine_mode.txt
echo "`date -u` install-complete" >> /root/state/mine_mode_log.txt

cd $START_DIR

echo 'install complete'